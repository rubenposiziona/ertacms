<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subfamilia extends Model
{
    protected $table = 'subfamilias';

	protected $fillable = [
    	'nombre', 'id_familia', 'activo',
    ];

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
}
