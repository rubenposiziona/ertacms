<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasFacturaCompra extends Model
{
    protected $table = 'lineas_factura_compra';

	//Claves ajenas
	public function order()
    {
        return $this->belongsTo('App\FacturaCompra','id_factura')->get();
    }
    public function product()
    {
        return $this->belongsTo('App\Producto','id_producto')->first();
    }
    public function format()
    {
        return $this->belongsTo('App\FormatosVenta','id_formato')->first();
    }
}
