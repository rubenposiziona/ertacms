<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedores';

	protected $fillable = [
    	'nif','nombre','email','direccion','c_postal','poblacion','provincia','pais','telefono','movil','activo',
    ];

    public function scopeActive($query) {
    	return $query->where('activo',1);
    }
}
