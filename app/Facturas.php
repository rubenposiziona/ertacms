<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturas extends Model
{
    protected $table = 'facturas';

    protected $dates = ['fecha_factura'];

    public function lines()
    {
        return $this->hasMany('App\LineasFactura','id_factura')->get();
    }
    public function state()
    {
        return $this->belongsTo('App\EstadosFactura','estado')->first();
    }
    public function payment()
    {
        return $this->belongsTo('App\FormasPago','formaPago')->first();
    }
}
