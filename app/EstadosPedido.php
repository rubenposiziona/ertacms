<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosPedido extends Model
{
    protected $table = 'estados_pedido';

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }

    //Pedidos
    public function orders()
    {
        return $this->hasMany('App\Pedidos','estado')->get();
    }
}
