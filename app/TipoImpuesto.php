<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoImpuesto extends Model
{
    protected $table = 'tipo_impuesto';

	protected $fillable = [
    	'tipo', 'activo',
    ];

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
}
