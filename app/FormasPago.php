<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormasPago extends Model
{
    protected $table = 'formas_pago';

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
}
