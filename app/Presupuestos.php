<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuestos extends Model
{
    protected $table = 'presupuestos';

	protected $fillable = [
    	'version', 'estado', 'cliente', 'fecha', 'valido_hasta', 'archivo', 'observaciones', 'subtotal', 'iva', 'total',
    ];

    protected $dates = ['fecha','valido_hasta'];

    //Lineas de presupuesto
    public function lines()
    {
        return $this->hasMany('App\LineasPresupuesto','id_presupuesto')->get();
    }
    public function client()
    {
        return $this->belongsTo('App\Cliente','cliente')->first();
    }
    public function state()
    {
        return $this->belongsTo('App\EstadosPresupuesto','estado')->first();
    }
    public static function getMyBudget($ident)
    {
        return Presupuestos::where("id_comercial",$ident)->get();
    }
}
