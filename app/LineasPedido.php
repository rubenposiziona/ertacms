<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasPedido extends Model
{
	protected $table = 'lineas_pedido';

    protected $fillable = [
        'id_pedido', 'id_producto', 'id_formato', 'cantidad', 'precio', 'impuesto', 'total',
    ];

	//Claves ajenas
	public function order()
    {
        return $this->belongsTo('App\Pedidos','id_pedido')->get();
    }
    public function product()
    {
        return $this->belongsTo('App\Producto','id_producto')->first();
    }
    public function format()
    {
        return $this->belongsTo('App\FormatosVenta','id_formato')->first();
    }
}
