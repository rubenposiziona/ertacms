<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Illuminate\Http\Request;

use Validator;
use Crypt;
use DB;

use App\Cliente;
use App\Comercial;
use App\FormasPago;
use App\ClientesXComercial;
use App\Pais;

class ClientesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','role:admin|comercial']);

        $this->middleware(function ($request, $next) {
            Session::put('section', 'clientes');
            return $next($request);
        });
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole("admin")){
            $clientes = Cliente::Active()->get();
        }else if(Auth::user()->hasRole("comercial")){
            $clientes = Comercial::comercialByUser(Auth::user()->id)->clientes();
        }

        return view('clientes.listado', compact('clientes'));
    }

    public function getClientSheet($id_cliente = null)
    {
        $nuevo          = null;
        if(!$id_cliente){
            $cliente        = new Cliente;
            $comercial_asig = "-1";
            $nuevo          = 1;
        }else{
            $cliente    = Cliente::Active()->find(Crypt::decrypt($id_cliente));
            if(!$cliente){
                return redirect('/clientes');
            }
            
            //Si ya existe comprobamos que este asignado al comercial actual
            $comercial_asig = ClientesXComercial::where('id_cliente',Crypt::decrypt($id_cliente))->first();
            if ($comercial_asig) {
                $cliente->comercial_asig = $comercial_asig->id_comercial;
            } else {
                $cliente->comercial_asig = '-1';
            }
        }

        $pago       = FormasPago::Active()->pluck('tipo', 'id')->toArray();
        $paises     = Pais::orderBy('nombre','asc')->pluck('nombre', 'id')->toArray();

        if(Auth::user()->hasRole("admin")){
            $comercial  = Comercial::Active()->pluck('nombre', 'id')->toArray();
        }else if(Auth::user()->hasRole("comercial")){
            $comercial  = Auth::user()->comercial()->id;
            if($cliente->comercial_asig != Auth::user()->comercial()->id && !$nuevo){
                Session::flash('alert-danger', "El cliente no está dentro de tu cartera.");
                return redirect('/clientes');
            }
        }

        return view('clientes.ficha', compact('cliente','pago','comercial','paises'));
    }

    public function save(Request $request) {

        //dd($request->all());
        
        $validator = Validator::make($request->all(), [
            'nombre'            => 'required|string',
            'nombre_comercial'  => 'required|string',
            'nif'               => 'required|string|max:30',
            'email'             => 'required|email',
            'direccion'         => 'required|string',
            'codigo_postal'     => 'string',
            'poblacion'         => 'string',
            'provincia'         => 'string',
        ]);

        if ($validator->fails()) {

            $errores = implode(' <br>', $validator->messages()->all());

            Session::flash('alert-danger', $errores);
            return redirect()->back()->withInput();
        } else {

            if ($request->id_cliente) {

                DB::beginTransaction();

                try {

                    $cliente = Cliente::find($request->id_cliente);
                    $cliente->nombre        	= $request->nombre;
                    $cliente->nombre_comercial  = $request->nombre_comercial;
                    $cliente->nif           	= $request->nif;
                    $cliente->email         	= $request->email;
                    $cliente->direccion     	= $request->direccion;
                    $cliente->c_postal      	= $request->codigo_postal;
                    $cliente->poblacion     	= $request->poblacion;
                    $cliente->provincia     	= $request->provincia;
                    $cliente->pais          	= $request->pais;
                    $cliente->telefono      	= $request->telefono;
                    $cliente->movil         	= $request->movil;
                    $cliente->observaciones 	= $request->observaciones;
                    $cliente->archivo       	= 0;
                    $cliente->formaPago     	= $request->forma_pago;
                    
                    //Actualizamos el cliente
                    if (!$cliente->save()) {
                        throw new Exception();
                    }

                    $existe_comercial =  ClientesXComercial::where('id_cliente',$request->id_cliente)
                                                            ->first();

                    if (!$existe_comercial) {
                        $comercial = ClientesXComercial::create(['id_cliente' => $request->id_cliente, 'id_comercial' => $request->comercial]);
                    } else {
                        $existe_comercial->id_comercial = $request->comercial;
                        $existe_comercial->save();
                    }

                    DB::commit();

                } catch (\Exception $e) {
                    dd($e);
                    DB::rollback();
                    Session::flash('alert-danger', '¡Error al intentar guardar el cliente!');
                    return redirect()->back()->withInput();
                }
                
                Session::flash('alert-success', '¡Datos guardados correctamente!');
                return redirect('clientes/ficha-cliente/'.Crypt::encrypt($cliente->id));

            } else {
            
                //Comprobamos si el cliente ya está registrado
                $existe = Cliente::Active()
                                    ->where('nif',$request->nif)
                                    ->first();
                if ($existe) {
                    Session::flash('alert-danger', '¡El NIF/CIF que intenta registrar ya pertenece a un cliente en nuestra base de datos!');
                    return redirect()->back()->withInput();
                }

                DB::beginTransaction();

                try {

                    $cliente = new Cliente;

                    $cliente->nombre            = $request->nombre;
                    $cliente->nombre_comercial  = $request->nombre_comercial;
                    $cliente->nif               = $request->nif;
                    $cliente->email             = $request->email;
                    $cliente->direccion         = $request->direccion;
                    $cliente->c_postal          = $request->codigo_postal;
                    $cliente->poblacion         = $request->poblacion;
                    $cliente->provincia         = $request->provincia;
                    $cliente->pais              = $request->pais;
                    $cliente->telefono          = $request->telefono;
                    $cliente->movil             = $request->movil;
                    $cliente->observaciones     = $request->observaciones;
                    $cliente->archivo           = 0;
                    $cliente->formaPago         = $request->forma_pago;
                    
                    //Actualizamos el cliente
                    if (!$cliente->save()) {
                        throw new Exception();
                    }

                    $comercial = ClientesXComercial::create(['id_cliente' => $cliente->id, 'id_comercial' => $request->comercial]);

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('alert-danger', '¡Error al intentar crear el cliente!');
                    return redirect()->back()->withInput();
                }

                Session::flash('alert-success', '¡Cliente creado correctamente!');
                return redirect('clientes/ficha-cliente/'.Crypt::encrypt($cliente->id));

            }

        }

    }

    public function deleteClient($id_cliente) {

        try {
            $cliente = Cliente::find($id_cliente);

            $cliente->update(['activo' => 0]);

        } catch (\Exception $e) {
            Session::flash('alert-danger', '¡Error al intentar eliminar el cliente!');
            return redirect()->back();
        }

        Session::flash('alert-success', '¡Cliente eliminado correctamente!');
        return redirect()->back();

    }

}
