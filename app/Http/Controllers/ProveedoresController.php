<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Proveedor;
use App\Pais;

use Validator;
use Session;
use Crypt;
use DB;

class ProveedoresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            Session::put('section', 'proveedores');
            return $next($request);
        });
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$proveedores   = Proveedor::Active()->get();

        return view('proveedores.listado', compact('proveedores'));

    }

    public function getNewSheet()
    {

        $proveedor  = new Proveedor;
        $paises     = Pais::orderBy('nombre','asc')->pluck('nombre', 'id')->toArray();

        return view('proveedores.ficha', compact('proveedor','paises'));
    }

    public function getClientSheet($id_proveedor)
    {
        $id_proveedor = Crypt::decrypt($id_proveedor);

        $proveedor  = Proveedor::Active()->find($id_proveedor);
        $paises     = Pais::orderBy('nombre','asc')->pluck('nombre', 'id')->toArray();

        return view('proveedores.ficha', compact('proveedor','paises'));
    }

    public function save(Request $request) {

        //dd($request->all());
        
        $validator = Validator::make($request->all(), [
            'nombre'            => 'required|string',
            'nif'               => 'required|string|max:30',
            'email'             => 'required|email',
            'direccion'         => 'required|string',
            'codigo_postal'     => 'string',
            'poblacion'         => 'string',
            'provincia'         => 'string',
        ]);

        if ($validator->fails()) {

            $errores = implode(' <br>', $validator->messages()->all());

            Session::flash('alert-danger', $errores);
            return redirect()->back()->withInput();
        } else {

            if ($request->id_proveedor) {

                DB::beginTransaction();

                try {

                    $proveedor = Proveedor::find($request->id_proveedor);
                    $proveedor->nombre        = $request->nombre;
                    $proveedor->nif           = $request->nif;
                    $proveedor->email         = $request->email;
                    $proveedor->direccion     = $request->direccion;
                    $proveedor->c_postal      = $request->codigo_postal;
                    $proveedor->poblacion     = $request->poblacion;
                    $proveedor->provincia     = $request->provincia;
                    $proveedor->pais          = $request->pais;
                    $proveedor->telefono      = $request->telefono;
                    $proveedor->movil         = $request->movil;
                    $proveedor->observaciones = $request->observaciones;
                    
                    //Actualizamos el proveedor
                    if (!$proveedor->save()) {
                        throw new Exception();
                    }

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('alert-danger', '¡Error al intentar guardar el proveedor!');
                    return redirect()->back()->withInput();
                }
                
                Session::flash('alert-success', '¡Datos guardados correctamente!');
                return redirect('proveedores/ficha-proveedor/'.Crypt::encrypt($proveedor->id));

            } else {

                //Comprobamos si el proveedor ya está registrado
                $existe = Proveedor::Active()
                                    ->where('nif',$request->nif)
                                    ->first();
                if ($existe) {
                    Session::flash('alert-danger', '¡El NIF/CIF que intenta registrar ya pertenece a un proveedor registrado!');
                    return redirect()->back()->withInput();
                }

                DB::beginTransaction();

                try {
                    
                    $proveedor = Proveedor::create(['nif' => $request->nif, 'nombre' => $request->nombre, 'email' => $request->email, 'direccion' => $request->direccion, 'c_postal' => $request->codigo_postal, 'poblacion' => $request->poblacion, 'provincia' => $request->provincia, 'pais' => $request->pais, 'recipient_zip' => $request->codigo_postal, 'telefono' => $request->telefono, 'movil' => $request->movil, 'activo' => '1']);

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('alert-danger', '¡Error al intentar crear el proveedor!');
                    return redirect()->back()->withInput();
                }

                Session::flash('alert-success', '¡Proveedor creado correctamente!');
                return redirect('proveedores/ficha-proveedor/'.Crypt::encrypt($proveedor->id));

            }

        }

    }

    public function deleteClient($id_proveedor) {

        try {
            $proveedor = Proveedor::find($id_proveedor);

            $proveedor->update(['activo' => 0]);

        } catch (\Exception $e) {
            Session::flash('alert-danger', '¡Error al intentar eliminar el proveedor!');
            return redirect()->back();
        }

        Session::flash('alert-success', '¡Proveedor eliminado correctamente!');
        return redirect()->back();

    }

}
