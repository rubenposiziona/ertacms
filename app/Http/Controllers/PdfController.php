<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LineasPedido;
use App\Pedidos;
use App\FormasPago;
use App\LineasFactura;
use App\Facturas;

use Crypt;

class PdfController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$id_pedido = 1;
    	$pedido = Pedidos::find($id_pedido);

    	//Generamos el html del pdf para mostrar por pantalla el diseño
    	$lineas_pedido = $pedido->lines();

		return view('pdf.albaran', compact('pedido','lineas_pedido'));

        //$view =  \View::make('pdf.albaran', compact('lineas_pedido'))->render();
        //$pdf = \App::make('dompdf.wrapper');
        //$pdf->loadHTML($view);
        //return $pdf->download('albaran_'.$id_pedido);
		//return $pdf->stream();

    }

    public function generateOrder($id_pedido)
    {

    	$pedido = Pedidos::find(Crypt::decrypt($id_pedido));

    	$lineas_pedido = $pedido->lines();

        $view =  \View::make('pdf.albaran', compact('pedido','lineas_pedido'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
		return $pdf->stream();
        //return $pdf->download('albaran_'.$id_pedido);

    }

    public function generateInvoice($id_factura)
    {

    	$factura = Facturas::find(Crypt::decrypt($id_factura));

    	$lineas_factura = $factura->lines();

        $view =  \View::make('pdf.factura', compact('factura','lineas_factura'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
		return $pdf->stream();
        //return $pdf->download('albaran_'.$id_pedido);

    }
}
