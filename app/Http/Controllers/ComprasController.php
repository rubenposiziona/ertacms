<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Proveedor;
use App\Producto;
use App\FormatosVenta;
use App\TipoImpuesto;
use App\FacturaCompra;
use App\LineasFacturaCompra;

use Auth;
use Exception;
use Validator;
use Session;
use Crypt;
use DB;

class ComprasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'compras');
            return $next($request);
        });
        
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas = FacturaCompra::all();

        return view('compras.listado', compact('facturas'));
    }

    public function getBillSheet($id_factura = null)
    {
        $proveedor  = Proveedor::Active()->pluck('nombre', 'id')->toArray();
        $productos  = Producto::Active()->select(DB::raw("CONCAT(referencia,' - ',descripcion) AS referencia"),'id')->pluck('referencia', 'id')->toArray();

        if(isset($id_factura)){
            $factura     = FacturaCompra::find(Crypt::decrypt($id_factura));
            if($factura){
                $factura->fechaFacturaFormat  = null;
                $factura->fechaVencimientoFormat = null;
                if($factura->fecha_factura){
                    $factura->fechaFacturaFormat   = $factura->fecha_factura->format("d/m/Y");
                }
                if($factura->fecha_vencimiento){
                    $factura->fechaVencimientoFormat   = $factura->fecha_vencimiento->format("d/m/Y");
                }
            }else{
                $factura = $this->newBill();
            }
        }else{
            $factura = $this->newBill();
        }
        $lineasFactura   = $factura->lines();

        if(!count($lineasFactura)){
            $lineasFactura[0]            = new LineasFacturaCompra;
            $lineasFactura[0]->impuesto  = 21;
            $lineasFactura[0]->formatos  = array();
        } else {
            foreach ($lineasFactura as $linea) {
                
                $linea->formatos = $linea->product()->formatos()->pluck('referencia', 'id')->toArray();

            }
        }

        return view('compras.factura', compact('proveedor','productos','factura','lineasFactura'));
    }

    private function newBill()
    {
        $factura = new FacturaCompra;

        $factura->fechaFacturaFormat      = date("d/m/Y");
        $factura->fechaVencimientoFormat     = null;

        return $factura;
    }

}
