<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comercial;
use App\User;
use App\Role;

use Validator;
use Session;
use Crypt;
use DB;
use Hash;


class ComercialesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'comerciales');
            return $next($request);
        });
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$comerciales = Comercial::Active()->get();
        return view('comerciales.listado', compact('comerciales'));
    }

    public function getNewSheet()
    {

        $comercial = new Comercial;

        return view('comerciales.ficha', compact('comercial'));
    }

    public function getSellerSheet($id_comercial)
    {
        $id_comercial = Crypt::decrypt($id_comercial);

        $comercial = Comercial::Active()->find($id_comercial);

        return view('comerciales.ficha', compact('comercial'));
    }

    public function save(Request $request) {

        //dd($request->all());
        
        $validator = Validator::make($request->all(), [
            'nombre'            => 'required|string',
            'nif'               => 'required|string|max:9',
            'email'             => 'required|email',
            'direccion'         => 'required|string',
            'codigo_postal'     => 'string',
            'poblacion'         => 'string',
            'provincia'         => 'string',
        ]);

        if ($validator->fails()) {

            $errores = implode(' <br>', $validator->messages()->all());

            Session::flash('alert-danger', $errores);
            return redirect()->back()->withInput();
        } else {

            if (Comercial::checkExists($request->nif,$request->id_comercial))
            {
                Session::flash('alert-danger', '¡El NIF/CIF que intenta registrar ya pertenece a un comercial en nuestra base de datos!');
                return redirect()->back()->withInput();
            }

            if ($request->id_comercial) {

                DB::beginTransaction();

                try {

                    $comercial = Comercial::find($request->id_comercial);
                    $comercial->nombre        = $request->nombre;
                    $comercial->nif           = $request->nif;
                    $comercial->email         = $request->email;
                    $comercial->direccion     = $request->direccion;
                    $comercial->c_postal      = $request->codigo_postal;
                    $comercial->poblacion     = $request->poblacion;
                    $comercial->provincia     = $request->provincia;
                    $comercial->telefono      = $request->telefono;
                    $comercial->movil         = $request->movil;
                    
                    //Actualizamos el comercial
                    if (!$comercial->save()) {
                        throw new Exception();
                    }

                    $user           = User::find($comercial->id_user);
                    $user->name     = $request->nif;
                    $user->email    = $request->email;

                    //Actualizamos el comercial
                    if (!$user->save()) {
                        throw new Exception();
                    }

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('alert-danger', '¡Error al intentar guardar el comercial!');
                    return redirect()->back()->withInput();
                }
                
                Session::flash('alert-success', '¡Datos guardados correctamente!');
                return redirect('comerciales/ficha-comercial/'.Crypt::encrypt($comercial->id));

            } else {

                DB::beginTransaction();

                try {
                    
                    $comercial = Comercial::create(['nif' => $request->nif, 'nombre' => $request->nombre, 'email' => $request->email, 'direccion' => $request->direccion, 'c_postal' => $request->codigo_postal, 'poblacion' => $request->poblacion, 'provincia' => $request->provincia, 'recipient_zip' => $request->codigo_postal, 'telefono' => $request->telefono, 'movil' => $request->movil, 'activo' => '1']);

                    $user           = new User;
                    $user->name     = $request->nif;
                    $user->email    = $request->email;
                    $user->password = Hash::make($request->nif);
                    
                    //Creamos el usuario
                    if (!$user->save()) {
                        throw new Exception();
                    }

                    //Asignamos usuario al comercial
                    $comercial->id_user = $user->id;
                    if (!$comercial->save()) {
                        throw new Exception();
                    }

                    $user->attachRole(Role::where("name","comercial")->first());

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('alert-danger', '¡Error al intentar crear el comercial!');
                    return redirect()->back()->withInput();
                }

                Session::flash('alert-success', '¡Comercial creado correctamente!');
                return redirect('comerciales/ficha-comercial/'.Crypt::encrypt($comercial->id));

            }

        }

    }

    public function deleteSeller($id_comercial) {

        try {

            $comercial  = Comercial::find($id_comercial);
            $comercial->update(['activo' => 0]);

            $user       = User::find($comercial->id_user);
            $user->update(['active' => 0]);

        } catch (\Exception $e) {
            Session::flash('alert-danger', '¡Error al intentar eliminar el comercial!');
            return redirect()->back();
        }

        Session::flash('alert-success', '¡Comercial eliminado correctamente!');
        return redirect()->back();

    }
}
