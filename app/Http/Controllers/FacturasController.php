<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EstadosFactura;
use App\FormasPago;
use App\Facturas;
use App\Cliente;
use App\Producto;

use Exception;
use Validator;
use Session;
use Crypt;
use DB;

class FacturasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'facturas');
            return $next($request);
        });
        
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $facturas = Facturas::all();

        return view('facturas.listado',compact('facturas'));
    }

    private function newBill()
    {
        $factura = new Facturas;

        return $factura;
    }

    public function getBillSheet($id_factura)
    {

        $factura = Facturas::find(Crypt::decrypt($id_factura));

        if(!$factura){
            Session::flash('alert-danger', 'Error al intentar acceder a la ficha de la factura');
            return redirect()->back();
        }

        $estados    = EstadosFactura::Active()->pluck('estado', 'id')->toArray();
        $pagos      = FormasPago::Active()->pluck('tipo', 'id')->toArray();

        return view('facturas.ficha-factura', compact('factura','estados','pagos'));
    }

    public function saveBill(Request $request)
    {
        DB::beginTransaction();

        try {

            $factura = Facturas::find($request->id_factura);

            if($factura) {

                $factura->estado    = $request->estado;
                $factura->formaPago = $request->forma_pago;
                $factura->pago_parcial = $request->cantidad_parcial;

                $factura->save();

            } else {
                Session::flash('alert-danger', 'Error al intentar guardar la factura');
                return redirect()->back();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert-danger', 'Error al intentar guardar la factura');
            return redirect()->back();
        }
        
        Session::flash('alert-success', '¡Datos guardados correctamente!');
        return redirect('facturas/ficha-factura/'.Crypt::encrypt($factura->id));

    }

}
