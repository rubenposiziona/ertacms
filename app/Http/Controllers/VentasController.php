<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Cliente;
use App\Comercial;
use App\Producto;
use App\FormatosXProducto;
use App\FormatosVenta;
use App\TipoImpuesto;
use App\Pedidos;
use App\Facturas;
use App\LineasPedido;
use App\Presupuestos;
use App\PresupuestosVersion;
use App\LineasPresupuesto;
use App\LineasPresupuestoVersion;
use App\LineasFactura;
use App\EstadosPedido;
use App\EstadosPresupuesto;
use App\Series;

use Auth;
use Exception;
use Validator;
use Session;
use Crypt;
use DB;

class VentasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'ventas');
            return $next($request);
        });
        
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole("admin")){

            $total = new Collection;

            $total->sumPending = Pedidos::sumTotalPending();
            $total->numPending = Pedidos::numPending();
            $total->sumAccept = Pedidos::sumTotalAccept();
            $total->numAccept = Pedidos::numAccept();

            $albaranes      = Pedidos::whereIn('estado',array(2, 5))->get();
            $pedidos        = Pedidos::whereNotIn('estado',array(2, 5))->get();
            $presupuestos   = Presupuestos::all();

            return view('ventas.listado',compact('albaranes','pedidos','presupuestos','total'));

        }else if(Auth::user()->hasRole("comercial")){

            $presupuestos   = Presupuestos::getMyBudget(Auth::user()->comercial()->id);

            return view('ventas.listado-comercial',compact('presupuestos'));

        }
    }

    public function getOrderSheet($id_pedido = null)
    {
        $estados    = EstadosPedido::Active()->pluck('estado', 'id')->toArray();
        $clientes   = Cliente::Active()->orderBy('nombre','ASC')->pluck('nombre', 'id')->toArray();
        $productos  = Producto::Active()->orderBy('referencia','ASC')->select(DB::raw("CONCAT(referencia,' - ',descripcion) AS referencia"),'id')->pluck('referencia', 'id')->toArray();

        if(isset($id_pedido)){
            $pedido = Pedidos::find(Crypt::decrypt($id_pedido));
            if($pedido){
                $pedido->fechaPedidoFormat  = null;
                $pedido->fechaEntregaFormat = null;
                if($pedido->fecha_pedido){
                    $pedido->fechaPedidoFormat = $pedido->fecha_pedido->format("d/m/Y");
                }
                if($pedido->fecha_entrega){
                    $pedido->fechaEntregaFormat = $pedido->fecha_entrega->format("d/m/Y");
                }
            }else{
                $pedido = $this->newOrder();
            }
        }else{
            $pedido = $this->newOrder();
        }
        $lineasPedido = $pedido->lines();

        if(!count($lineasPedido)){
            $lineasPedido[0]            = new LineasPedido;
            $lineasPedido[0]->impuesto  = 21;
            $lineasPedido[0]->formatos  = array();
        } else {
            foreach ($lineasPedido as $linea) {
                
                $linea->formatos = $linea->product()->formatos()->pluck('referencia', 'id')->toArray();

            }
        }

        return view('ventas.pedido', compact('estados','clientes','productos','pedido','lineasPedido'));
    }

    private function newOrder()
    {
        $pedido = new Pedidos;

        $pedido->estado                 = 1;
        $pedido->fechaPedidoFormat      = date("d/m/Y");
        $pedido->fechaEntregaFormat     = null;

        return $pedido;
    }

    public function saveOrder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'cliente'        => 'required',
            'fecha_pedido'   => 'required', 
        ]);

        if ($validator->fails()) {

            $errores = implode(' <br>', $validator->messages()->all());

            Session::flash('alert-danger', $errores);
            return redirect()->back();

        } else {

            DB::beginTransaction();

            try {

                if ($request->id_pedido) {
                    $pedido     = Pedidos::find($request->id_pedido);
                }else{
                    $pedido     = new Pedidos;
                }

                if($pedido->estado != 1 && $request->id_pedido){
                    throw new Exception('El estado de este pedido no permite modificaciones');
                }

                $pedido->estado             = $request->estado;
                $pedido->observaciones      = $request->observaciones;
                $pedido->cliente            = $request->cliente;
                $client                     = Cliente::find($request->cliente);
                if (!$client) {
                    throw new Exception('Debe seleccionar un cliente');
                }
                $pedido->nombre             = $client->nombre;
                $pedido->cif                = $client->nif;
                $pedido->nom_comercial      = $client->nombre;
                $pedido->direccion          = $client->direccion;
                $pedido->poblacion          = $client->poblacion;
                $pedido->provincia          = $client->provincia;
                $pedido->c_postal           = $client->c_postal;
                $pedido->telefono           = $client->telefono;
                $pedido->movil              = $client->movil;
                $pedido->fax                = $client->movil;
                $pedido->email              = $client->email;
                $pedido->formaPago          = $client->formaPago;

                $fecha_pedido = $request->fecha_pedido;
                if ($request->fecha_pedido) {
                    $fecha_pedido = date("Y-m-d", strtotime(str_replace('/', '-', $request->fecha_pedido)));
                }

                if ($request->fecha_entrega) {
                    $fecha_entrega = date("Y-m-d", strtotime(str_replace('/', '-', $request->fecha_entrega)));
                } else {
                    $fecha_entrega = $fecha_pedido;
                }

                $pedido->fecha_pedido  = $fecha_pedido;
                $pedido->fecha_entrega = $fecha_entrega;
                
                //Guardamos el pedido
                if ($pedido->save()) {

                    $suma_subtotal  = 0;
                    $iva_total      = 0;
                    $suma_total     = 0;

                    foreach ($request->idLinea as $key => $linea) {
                        
                        //Hay producto
                        if ($request->producto[$key] != '-1' && $request->total[$key] > 0) {
                            
                            //Compruebo para modificar o crear la linea de pedido
                            if($linea > 0){
                                $lineaPedido = LineasPedido::find($linea);
                            }else{
                                $lineaPedido = new LineasPedido;
                            }
                            if($lineaPedido){
                                $lineaPedido->id_pedido    = $pedido->id;
                                $lineaPedido->id_producto  = $request->producto[$key];
                                $lineaPedido->id_formato   = $request->formato[$key];
                                $lineaPedido->cantidad     = $request->cantidad[$key];
                                $lineaPedido->precio       = $request->precio[$key];
                                $lineaPedido->impuesto     = $request->impuesto[$key];
                                $lineaPedido->total        = $request->total[$key];

                                $suma_subtotal   += $request->precio[$key] * $request->cantidad[$key];
                                $iva_total       += $request->precio[$key] * $request->cantidad[$key] * ($request->impuesto[$key]/100);
                                $suma_total      += $request->precio[$key] * $request->cantidad[$key] * (1 + ($request->impuesto[$key]/100));
                            }
                            $lineaPedido->save();

                        }else{

                            if($linea > 0){
                                $lineaPedido = LineasPedido::find($linea);
                                if($lineaPedido){
                                    $lineaPedido->delete();
                                }
                            }

                        }

                    }//end foreach idLinea

                    $pedido->num_pedido = $pedido->id;
                    $pedido->subtotal   = $suma_subtotal;
                    $pedido->iva        = $iva_total;
                    $pedido->total      = $suma_total;
                    $pedido->save();

                }

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                Session::flash('alert-danger', $e->getMessage());
                return redirect()->back();
            }
            
            Session::flash('alert-success', '¡Datos guardados correctamente!');
            return redirect('ventas/ficha-pedido/'.Crypt::encrypt($pedido->id));

        }
    }

    public function getBudgetSheet($id_presupuesto = null)
    {
        $estados    = EstadosPresupuesto::Active()->pluck('estado', 'id')->toArray();
        $clientes   = Cliente::Active()->orderBy('nombre','ASC')->pluck('nombre', 'id')->toArray();
        $productos  = Producto::Active()->orderBy('referencia','ASC')->select(DB::raw("CONCAT(referencia,' - ',descripcion) AS referencia"),'id')->pluck('referencia', 'id')->toArray();

        //Usamos solo los clientes asociados al comercial
        if(Auth::user()->hasRole("comercial"))
        {
            $clientes   = Comercial::comercialByUser(Auth::user()->id)->clientes()->pluck('nombre', 'id')->toArray();
            $estados    = EstadosPresupuesto::ActiveComercial()->pluck('estado', 'id')->toArray();
        }

        if(isset($id_presupuesto)){
            $presupuesto     = Presupuestos::find(Crypt::decrypt($id_presupuesto));

            if($presupuesto){
                //Si no es un presupuesto del comercial redirigimos al listado
                if(Auth::user()->hasRole("comercial"))
                {
                    if($presupuesto->id_comercial != Auth::user()->comercial()->id)
                    {
                        Session::flash('alert-danger', "No tienes permisos para acceder al presupuesto.");
                        return redirect("ventas");
                    }
                }

                $presupuesto->fechaPresupuestoFormat  = null;
                $presupuesto->fechaValidoFormat = null;
                if($presupuesto->fecha){
                    $presupuesto->fechaPresupuestoFormat = $presupuesto->fecha->format("d/m/Y");
                }
                if($presupuesto->valido_hasta){
                    $presupuesto->fechaValidoFormat = $presupuesto->valido_hasta->format("d/m/Y");
                }
            }else{
                $presupuesto = $this->newBudget();
            }
        }else{
            $presupuesto = $this->newBudget();
        }
        $lineasPresupuesto = $presupuesto->lines();

        if(!count($lineasPresupuesto)){
            $lineasPresupuesto[0]            = new LineasPresupuesto;
            $lineasPresupuesto[0]->impuesto  = 21;
            $lineasPresupuesto[0]->formatos  = array();
        } else {
            foreach ($lineasPresupuesto as $linea) {
                
                $linea->formatos = $linea->product()->formatos()->pluck('referencia', 'id')->toArray();

            }
        }

        return view('ventas.presupuesto', compact('estados','clientes','productos','presupuesto','lineasPresupuesto'));
    }

    private function newBudget()
    {
        $presupuesto = new Presupuestos;

        $presupuesto->id_comercial              = "-1";
        if(Auth::user()->hasRole("comercial")){
            $presupuesto->id_comercial              = Auth::user()->comercial()->id;
        }
        $presupuesto->estado                    = 1;
        $presupuesto->version                   = 1;
        $presupuesto->fechaPresupuestoFormat    = date("d/m/Y");
        $presupuesto->fechaValidoFormat         = null;

        return $presupuesto;
    }

    public function saveBudget(Request $request)
    {

        //dd($request->all());

        $validator = Validator::make($request->all(), [
            'cliente' => 'required',
            'fecha'   => 'required', 
        ]);

        if ($validator->fails()) {

            $errores = implode(' <br>', $validator->messages()->all());

            Session::flash('alert-danger', $errores);
            return redirect()->back();

        } else {

            DB::beginTransaction();

            try {

                if ($request->id_presupuesto) {
                    $presupuesto     = Presupuestos::find($request->id_presupuesto);
                    $presupuesto_v   = PresupuestosVersion::where("id_presupuesto",$request->id_presupuesto)
                                                            ->where("version",$request->version)->first();

                    if(!$presupuesto_v){
                        $presupuesto_v = new PresupuestosVersion;
                    }
                }else{
                    $presupuesto = new Presupuestos;
                    $presupuesto_v = new PresupuestosVersion;
                }

                if($presupuesto->estado != 1 && $request->id_presupuesto){
                    throw new Exception('El estado de este presupuesto no permite modificaciones');
                }

                $presupuesto->id_comercial       = $request->id_comercial;
                $presupuesto->estado             = $request->estado;
                $presupuesto->observaciones      = $request->observaciones;
                $presupuesto->version            = $request->version;
                $presupuesto->cliente            = $request->cliente;
                $client                          = Cliente::find($request->cliente);
                if (!$client) {
                    throw new Exception('Debe seleccionar un cliente');
                }
                $presupuesto->nombre             = $client->nombre;
                $presupuesto->cif                = $client->nif;
                $presupuesto->nom_comercial      = $client->nombre;
                $presupuesto->direccion          = $client->direccion;
                $presupuesto->poblacion          = $client->poblacion;
                $presupuesto->provincia          = $client->provincia;
                $presupuesto->c_postal           = $client->c_postal;
                $presupuesto->telefono           = $client->telefono;
                $presupuesto->movil              = $client->movil;
                $presupuesto->fax                = $client->movil;
                $presupuesto->email              = $client->email;
                $presupuesto->formaPago          = $client->formaPago;

                $fecha = $request->fecha;
                if ($request->fecha) {
                    $fecha = date("Y-m-d", strtotime(str_replace('/', '-', $request->fecha)));
                }

                if ($request->valido_hasta) {
                    $valido_hasta = date("Y-m-d", strtotime(str_replace('/', '-', $request->valido_hasta)));
                } else {
                    $valido_hasta = $fecha;
                }

                $presupuesto->fecha         = $fecha;
                $presupuesto->valido_hasta  = $valido_hasta;

                //guardamos la versión del presupuesto
                $presupuesto_v->version         = $request->version;
                $presupuesto_v->fecha           = $fecha;
                $presupuesto_v->valido_hasta    = $valido_hasta;
                $presupuesto_v->observaciones   = $request->observaciones;
                
                //Guardamos el presupuesto
                if ($presupuesto->save() && $presupuesto_v->save()) {

                    $suma_subtotal  = 0;
                    $iva_total      = 0;
                    $suma_total     = 0;

                    foreach ($request->idLinea as $key => $linea) {
                        
                        //Hay producto
                        if ($request->producto[$key] != '-1' && $request->total[$key] > 0) {
                            
                            //Compruebo para modificar o crear la linea de presupuesto
                            if($linea > 0){
                                $lineaPresupuesto = LineasPresupuesto::find($linea);
                                $lineaPresupuestoV = LineasPresupuestoVersion::where('id_presupuesto_v',$presupuesto_v->id)
                                                                                ->where('id_linea_presupuesto',$linea)->first();
                                if(!$lineaPresupuestoV){
                                    $lineaPresupuestoV = new LineasPresupuestoVersion;
                                }
                                
                            }else{
                                $lineaPresupuesto = new LineasPresupuesto;
                                $lineaPresupuestoV = new LineasPresupuestoVersion;
                            }

                            if($lineaPresupuesto){
                                $lineaPresupuesto->id_presupuesto    = $presupuesto->id;
                                $lineaPresupuesto->id_producto  = $request->producto[$key];
                                $lineaPresupuesto->id_formato   = $request->formato[$key];
                                $lineaPresupuesto->descuento    = $request->descuento[$key];
                                $lineaPresupuesto->cantidad     = $request->cantidad[$key];
                                $lineaPresupuesto->precio       = $request->precio[$key];
                                $lineaPresupuesto->impuesto     = $request->impuesto[$key];
                                $lineaPresupuesto->total        = $request->total[$key];

                                $suma_subtotal   += (1 - $request->descuento[$key]/100) * $request->precio[$key] * $request->cantidad[$key];
                                $iva_total       += (1 - $request->descuento[$key]/100) * $request->precio[$key] * $request->cantidad[$key] * ($request->impuesto[$key]/100);
                                $suma_total      += (1 - $request->descuento[$key]/100) * $request->precio[$key] * $request->cantidad[$key] * (1 + ($request->impuesto[$key]/100));
                            }
                            $lineaPresupuesto->save();

                            if($lineaPresupuestoV){
                                $lineaPresupuestoV->id_presupuesto_v  = $presupuesto_v->id;
                                $lineaPresupuestoV->id_linea_presupuesto  = $lineaPresupuesto->id;
                                $lineaPresupuestoV->id_producto     = $request->producto[$key];
                                $lineaPresupuestoV->id_formato      = $request->formato[$key];
                                $lineaPresupuestoV->descuento       = $request->descuento[$key];
                                $lineaPresupuestoV->cantidad        = $request->cantidad[$key];
                                $lineaPresupuestoV->precio          = $request->precio[$key];
                                $lineaPresupuestoV->impuesto        = $request->impuesto[$key];
                                $lineaPresupuestoV->total           = $request->total[$key];

                            }
                            $lineaPresupuestoV->save();

                        }else{

                            if($linea > 0){
                                $lineaPresupuesto = LineasPresupuesto::find($linea);
                                if($lineaPresupuesto){
                                    $lineaPresupuesto->delete();
                                }
                                $lineaPresupuestoV = LineasPresupuestoVersion::where('id_presupuesto_v',$presupuesto_v->id)
                                                                                ->where('id_linea_presupuesto',$linea)->first();
                                if($lineaPresupuestoV){
                                    $lineaPresupuestoV->delete();
                                }
                            }

                        }

                    }//end foreach idLinea

                    $presupuesto->num_presupuesto = $presupuesto->id;
                    $presupuesto->subtotal   = $suma_subtotal;
                    $presupuesto->iva        = $iva_total;
                    $presupuesto->total      = $suma_total;
                    $presupuesto->save();

                    $presupuesto_v->id_presupuesto  = $presupuesto->id;
                    $presupuesto_v->subtotal        = $suma_subtotal;
                    $presupuesto_v->iva             = $iva_total;
                    $presupuesto_v->total           = $suma_total;
                    $presupuesto_v->save();

                }

                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                Session::flash('alert-danger', $e->getMessage());
                return redirect()->back();
            }
            
            Session::flash('alert-success', '¡Datos guardados correctamente!');
            return redirect('ventas/ficha-presupuesto/'.Crypt::encrypt($presupuesto->id));

        }
    }

    //Obtenemos el histórico de versiones de un presupuesto
    public function budgetVersions($id_presupuesto)
    {
        $id_presup      = Crypt::decrypt($id_presupuesto);
        $presupuesto    = Presupuestos::find($id_presup);
        $presupuestos   = PresupuestosVersion::where('id_presupuesto',$id_presup)->get();
        foreach ($presupuestos as $presup) {
            $presup->linea = LineasPresupuestoVersion::where('id_presupuesto_v',$presup->id)->get();
        }
        return view('ventas.historico', compact('presupuesto','presupuestos'));
    }

    public function generateOrder(Request $request)
    {

        DB::beginTransaction();

        try {

            $presupuesto = Presupuestos::find($request->id_presupuesto);

            $pedido = Pedidos::create(['estado' => 1, 'num_pedido' => NULL, 'cliente' => $presupuesto->cliente, 'fecha_pedido' => date("Y-m-d"), 'fecha_entrega' => NULL, 'archivo' => NULL, 'observaciones' => $presupuesto->observaciones, 'subtotal' => $presupuesto->subtotal, 'iva' => $presupuesto->iva, 'total' => $presupuesto->total, 'nombre' => $presupuesto->nombre, 'cif' => $presupuesto->cif, 'nom_comercial' => $presupuesto->nom_comercial, 'direccion' => $presupuesto->direccion, 'poblacion' => $presupuesto->poblacion, 'provincia' => $presupuesto->provincia, 'c_postal' => $presupuesto->c_postal, 'telefono' => $presupuesto->telefono, 'movil' => $presupuesto->movil, 'fax' => $presupuesto->fax, 'email' => $presupuesto->email, 'formaPago' => $presupuesto->formaPago, 'id_presupuesto' => $presupuesto->id]);

            $lineasPresupuesto = LineasPresupuesto::where('id_presupuesto',$presupuesto->id)->get();

            foreach ($lineasPresupuesto as $linea) {

                $precio = $linea->precio * (1 - $linea->descuento/100);

                $linea_p = LineasPedido::create(['id_pedido' => $pedido->id, 'id_producto' => $linea->id_producto, 'id_formato' => $linea->id_formato, 'cantidad' => $linea->cantidad, 'precio' => $precio, 'impuesto' => $linea->impuesto, 'total' => $linea->total]);
                
            }

            $pedido->num_pedido = $pedido->id;
            $pedido->save();

            $presupuesto->estado = 5;
            $presupuesto->exportado = 1;
            $presupuesto->save();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert-danger', '¡Error al intentar exportar el presupuesto!');
            return redirect()->back();
        }
        
        Session::flash('alert-success', '¡Presupuesto exportado a pedidos correctamente!');
        return redirect('ventas');

    }

    //Generamos una factura sobre un pedido
    public function generateBill(Request $request)
    {
        DB::beginTransaction();

        try {

            $pedido         = Pedidos::findOrFail($request->id_pedido);

            $pedido->update(['estado' => 5]);

            $serie          = Series::Active()->where('defecto',1)->first();

            $ult_factura    = Facturas::where('serie',$serie->id)->orderBy('id','DESC')->first(['numero']);


            if($ult_factura) {
                $num = ($ult_factura->numero) + 1;
            } else {
                $num = 1;
            }

            $num_factura = $serie->codigo."/".date('y').str_pad($num, 4, "0", STR_PAD_LEFT);

            $factura = new Facturas;

            $factura->estado = 1;
            $factura->serie = $serie->id;
            $factura->numero = $num;
            $factura->num_factura = $num_factura;
            $factura->fecha_factura = date("Y-m-d");
            $factura->subtotal = $pedido->subtotal;
            $factura->iva = $pedido->iva;
            $factura->total = $pedido->total;
            $factura->nombre = $pedido->nombre;
            $factura->cif = $pedido->cif;
            $factura->nom_comercial = $pedido->nom_comercial;
            $factura->direccion = $pedido->direccion;
            $factura->poblacion = $pedido->poblacion;
            $factura->provincia = $pedido->provincia;
            $factura->c_postal = $pedido->c_postal;
            $factura->telefono = $pedido->telefono;
            $factura->fax = $pedido->fax;
            $factura->email = $pedido->email;
            $factura->formaPago = $pedido->formaPago;
            $factura->id_pedido = $pedido->id;

            $factura->save();

            $lineasPedido = LineasPedido::where('id_pedido',$pedido->id)->get();

            foreach ($lineasPedido as $linea) {

                $linea_p = LineasFactura::create(['id_factura' => $factura->id, 'id_producto' => $linea->id_producto, 'id_formato' => $linea->id_formato, 'cantidad' => $linea->cantidad, 'precio' => $linea->precio, 'impuesto' => $linea->impuesto, 'total' => $linea->total]);
                
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert-danger', '¡Error al intentar generar la factura!');
            return redirect()->back();
        }
        
        Session::flash('alert-success', '¡Factura generada correctamente!');
        return redirect('facturas');

    }

    //Cargar por Ajax los formatos para un producto en concreto
    public function ajaxFormats(Request $request)
    {
        $producto = Producto::find($request->idProducto);
        $form_prod = FormatosXProducto::where('id_producto',$request->idProducto)->first(['precio']);
        if($producto){
            $formatos = $producto->formatos();
            $html     = "";
            foreach($formatos as $formato){
                $html .= "<option value=".$formato->id.">".$formato->referencia."</option>";
            }
            return response()->json(['html' => $html,'precio' => $form_prod->precio]);
        }else{
            return response()->json(['html' => "",'precio' => '0.00']);
        }
    }

    public function ajaxPrice(Request $request)
    {

        $form_prod = FormatosXProducto::where('id_producto',$request->idProducto)->where('id_formato',$request->idFormato)->first(['precio']);
        if($form_prod){
            $html = $form_prod->precio;
            return response()->json(['html' => $html]);
        }else{
            return response()->json(['html' => "0.00"]);
        }
    }
}
