<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Khill\Lavacharts\Lavacharts;

use Session;

class InformesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'informes');
            return $next($request);
        });
        
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lava = new Lavacharts; // See note below for Laravel

        $stock = $lava->DataTable();
       
        $stock->addDateColumn('Year')
             ->addNumberColumn('SN 500')
             ->addNumberColumn('SN 150')
             ->setDateTimeFormat('Y')
             ->addRow(['2014', 5000, 4000])
             ->addRow(['2015', 8700, 4600])
             ->addRow(['2016', 12200, 11200])
             ->addRow(['2017', 1300, 540]);

        $lava->ColumnChart('Total', $stock, [
            'title' => 'Ventas (litros)',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);


        return view('informes.listado',compact('lava'));
    }
}
