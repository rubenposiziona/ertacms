<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Illuminate\Http\Request;

use App\Section;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'dashboard');
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        if($user->hasRole("admin")){
            return view('home');
        }else if($user->hasRole("comercial")){
            return view('home-comercial');
        }

    }
}
