<?php

namespace App\Http\Controllers;

use App\FormatosXProducto;
use App\FormatosVenta;
use App\Composicion;
use App\Subfamilia;
use App\Producto;

use Illuminate\Http\Request;
use Exception;
use Validator;
use Session;
use Crypt;
use DB;
use Auth;

class ProductosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            Session::put('section', 'productos');
            return $next($request);
        });
    }

    /**
     * Show the application data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    	$productos = FormatosXProducto::leftjoin('productos', 'id_producto', '=', 'productos.id')
                                        ->leftjoin('formatos_venta', 'id_formato', '=', 'formatos_venta.id')
                                        ->where('precio','>',0)
                                        ->get(['productos.id','productos.referencia','descripcion','precio','formatos_venta.referencia as formato']);

        return view('productos.listado', compact('productos'));
    }

    public function getNewSheet()
    {

        if(Auth::user()->can('create-product')){

            $producto = new Producto;

            $formatos = FormatosVenta::Active()->get(['referencia', 'id']);

            $subfamilia = Subfamilia::Active()->pluck('nombre', 'id')->toArray();

            $composicion = Composicion::Active()->pluck('nombre', 'id')->toArray();

            return view('productos.ficha', compact('producto','formatos','subfamilia','composicion'));

        }else{

            return redirect("productos");

        }
    }

    public function getProductSheet($id_producto = null)
    {
        $id_producto = Crypt::decrypt($id_producto);

        $producto = Producto::Active()->find($id_producto);

        $formatos = FormatosVenta::Active()->get(['referencia', 'id']);

        foreach ($formatos as $formato) {
            $formato_prod = FormatosXProducto::where('id_producto',$id_producto)->where('id_formato',$formato->id)->first(['precio']);
            if ($formato_prod) {
                $formato->precio = $formato_prod->precio;
            } else {
                $formato->precio = '';
            }
        }

        $subfamilia = Subfamilia::Active()->pluck('nombre', 'id')->toArray();

        $composicion = Composicion::Active()->pluck('nombre', 'id')->toArray();

        return view('productos.ficha', compact('producto','formatos','subfamilia','composicion'));
    }

    public function save(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'referencia'        => 'required|string',
        ]);

        if ($validator->fails()) {

            $errores = implode(' <br>', $validator->messages()->all());

            Session::flash('alert-danger', $errores);
            return redirect()->back()->withInput();
        } else {

            if ($request->id_producto) {

                if(Auth::user()->can('update-product')){
                    DB::beginTransaction();

                    try {

                        $producto = Producto::find($request->id_producto);
                        $producto->referencia       = $request->referencia;
                        $producto->id_subfamilia    = $request->familia;
                        $producto->id_composicion   = $request->composicion;
                        $producto->descripcion      = $request->descripcion;
                        
                        //Actualizamos el producto
                        if ($producto->save()) {
                            
                            //Comprobamos el precio por formato
                            $asig = 0;
                            foreach ($request->formato as $key => $formato) {
                                
                                if ($request->get('precio_'.$key) != NULL) {
                                    $asig = 1;
                                }
                                    
                                $formato_producto = FormatosXProducto::where('id_producto',$producto->id)->where('id_formato',$key)->first();

                                //Modificamos el precio por formato
                                if ($formato_producto) {

                                    $formato_producto->precio = $request->get('precio_'.$key);

                                    if (!$formato_producto->save()) {
                                        throw new Exception('¡Error al intentar guardar el producto!');
                                    }

                                } else {

                                    $formato_producto = FormatosXProducto::create(['id_producto' => $producto->id, 'id_formato' => $key, 'precio' => $request->get('precio_'.$key)]);

                                }

                            }

                            if (!$asig) {
                                throw new Exception('¡Debe asignarle precio al menos a un formato!');
                            }

                        } else {
                            throw new Exception('¡Error al intentar guardar el producto!');
                        }

                        DB::commit();

                    } catch (\Exception $e) {
                        DB::rollback();
                        Session::flash('alert-danger', $e->getMessage());
                        return redirect()->back()->withInput();
                    }
                    
                    Session::flash('alert-success', '¡Datos guardados correctamente!');
                    return redirect('productos/ficha-producto/'.Crypt::encrypt($producto->id));
                } //Permission to update-product

            } else {

                if(Auth::user()->can('create-product')){
                    DB::beginTransaction();

                    try {

                        //Comprobamos si el producto ya está registrado
                        $existe = Producto::Active()
                                            ->where('referencia',$request->referencia)
                                            ->first();
                        if ($existe) {
                            Session::flash('alert-danger', '¡La referencia que intenta registrar ya pertenece a un producto en nuestra base de datos!');
                            return redirect()->back()->withInput();
                        }

                        $producto = Producto::create(['referencia' => $request->referencia, 'descripcion' => $request->descripcion, 'id_subfamilia' => $request->familia, 'id_composicion' => $request->composicion]);


                        //Comprobamos el precio por formato
                        $asig = 0;
                        foreach ($request->formato as $key => $formato) {
                            
                            if ($request->get('precio_'.$key) != NULL) {
                                
                                $asig = 1;

                                $formato_producto = FormatosXProducto::create(['id_producto' => $producto->id, 'id_formato' => $key, 'precio' => $request->get('precio_'.$key)]);

                            }

                        }

                        if (!$asig) {
                            throw new Exception('¡Debe asignarle precio al menos a un formato!');
                        }

                        DB::commit();

                    } catch (\Exception $e) {
                        DB::rollback();
                        Session::flash('alert-danger', $e->getMessage());
                        return redirect()->back()->withInput();
                    }

                    Session::flash('alert-success', '¡Producto creado correctamente!');
                    return redirect('productos/ficha-producto/'.Crypt::encrypt($producto->id));
                }
            }

        }

    }

    public function deleteProduct($id_producto) {

        if(Auth::user()->can('delete-product')){
            try {
                $producto = Producto::find($id_producto);

                $producto->update(['activo' => 0]);

            } catch (\Exception $e) {
                Session::flash('alert-danger', '¡Error al intentar eliminar el producto!');
                return redirect()->back();
            }

            Session::flash('alert-success', '¡Producto eliminado correctamente!');
            return redirect()->back();
        }
    }
}
