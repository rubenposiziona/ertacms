<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';

	protected $fillable = [
    	'referencia', 'descripcion', 'id_subfamilia', 'id_composicion', 'activo',
    ];

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
    public function linesOrder()
    {
        return $this->hasMany('App\LineasPedido','id_producto')->get();
    }
    public function formatos()
    {
        return $this->belongsToMany('\App\FormatosVenta','formatos_x_producto','id_producto','id_formato')->get();
    }

}
