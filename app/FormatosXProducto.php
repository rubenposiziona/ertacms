<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormatosXProducto extends Model
{
    protected $table = 'formatos_x_producto';

	protected $fillable = [
    	'id_producto', 'id_formato', 'precio',
    ];

}
