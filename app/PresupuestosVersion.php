<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresupuestosVersion extends Model
{
    protected $table = 'presupuestos_version';

	protected $fillable = [
    	'version', 'id_presupuesto', 'fecha', 'valido_hasta', 'observaciones', 'subtotal', 'iva', 'total',
    ];

    protected $dates = ['fecha','valido_hasta'];

    //Lineas de presupuesto
    public function lines()
    {
        return $this->hasMany('App\LineasPresupuestoVersion','id_presupuesto_v')->get();
    }
}
