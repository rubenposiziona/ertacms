<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosPresupuesto extends Model
{
    protected $table = 'estados_presupuesto';

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }

    public function scopeActiveComercial($query) {

    	return $query->where('activo',1)->where('activo_comercial',1);
    }

    //Pedidos
    public function orders()
    {
        return $this->hasMany('App\Presupuestos','estado')->get();
    }
}
