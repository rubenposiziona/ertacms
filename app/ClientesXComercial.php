<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientesXComercial extends Model
{
    protected $table = 'clientes_x_comercial';

	protected $fillable = [
    	'id_cliente', 'id_comercial',
    ];
}
