<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';

	protected $fillable = [
    	'nif','nombre','nombre_comercial','email','direccion','c_postal','poblacion','provincia','pais','telefono','movil','observaciones','archivo','formaPago','activo',
    ];

    public function scopeActive($query) {
    	return $query->where('activo',1);
    }

    public function comerciales()
    {
        return $this->belongsToMany('App\Comercial','clientes_x_comercial','id_cliente','id_comercial')->get();
    }
}
