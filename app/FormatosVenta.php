<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormatosVenta extends Model
{
    protected $table = 'formatos_venta';

	protected $fillable = [
    	'referencia', 'cantidad', 'activo',
    ];

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
    public function products()
    {
        return $this->belongsToMany('\App\Producto','formatos_x_producto','id_formato','id_producto')->get();
    }
}
