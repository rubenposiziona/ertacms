<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Composicion extends Model
{
    protected $table = 'composicion';

	protected $fillable = [
    	'nombre', 'sn500',  'sn150', 'activo',
    ];

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
}
