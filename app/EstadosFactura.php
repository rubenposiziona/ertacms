<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosFactura extends Model
{
    protected $table = 'estados_factura';

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
}
