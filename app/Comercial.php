<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercial extends Model
{
    protected $table = 'comerciales';

	protected $fillable = [
    	'nif', 'nombre', 'email', 'direccion', 'c_postal', 'poblacion', 'provincia', 'telefono', 'movil', 'activo',
    ];

    public function scopeActive($query)
    {
    	return $query->where('activo',1);
    }

    public static function checkExists($nif,$identComercial)
    {
    	return Comercial::Active()->where('nif',$nif)->where("id","!=",$identComercial)->first();
    }

    public function clientes()
    {
        return $this->belongsToMany('App\Cliente','clientes_x_comercial','id_comercial','id_cliente')->get();
    }

    public static function comercialByUser($usuario)
    {
    	return Comercial::where("id_user",$usuario)->first();
    }

    public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }
}
