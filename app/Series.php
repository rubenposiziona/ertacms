<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    protected $table = 'series';

    public function scopeActive($query) {

    	return $query->where('activo',1);
    }
}
