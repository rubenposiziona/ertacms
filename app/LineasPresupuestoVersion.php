<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasPresupuestoVersion extends Model
{
    protected $table = 'lineas_presupuesto_version';

    public function product()
    {
        return $this->belongsTo('App\Producto','id_producto')->first();
    }
    public function format()
    {
        return $this->belongsTo('App\FormatosVenta','id_formato')->first();
    }
}
