<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Section extends Model
{
    protected $table = 'sections';

    public static function sectionByRole($identRol)
    {
        $secciones = DB::select('select s.* from sections s inner join section_role sr ON sr.section_id = s.id WHERE sr.role_id = ? ORDER BY position ASC', array($identRol));
        return $secciones;
    }

    public function roles()
    {
        //Final Model - Intermediate table - FK current model - FK another model
        return $this->belongsToMany('App\Role','section_role','section_id','role_id')->where("active",1);
    }
}
