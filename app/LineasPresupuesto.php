<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasPresupuesto extends Model
{
    protected $table = 'lineas_presupuesto';

	//Claves ajenas
	public function order()
    {
        return $this->belongsTo('App\Pedidos','id_pedido')->get();
    }
    public function product()
    {
        return $this->belongsTo('App\Producto','id_producto')->first();
    }
    public function format()
    {
        return $this->belongsTo('App\FormatosVenta','id_formato')->first();
    }
}
