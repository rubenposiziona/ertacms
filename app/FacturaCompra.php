<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaCompra extends Model
{
    protected $table = 'factura_compra';

	protected $fillable = [
    	'num_factura', 'id_proveedor', 'fecha_factura', 'fecha_vencimiento', 'archivo', 'observaciones', 'subtotal', 'iva', 'total',
    ];

    protected $dates = ['fecha_factura','fecha_vencimiento'];

    //Lineas de factura
    public function lines()
    {
        return $this->hasMany('App\LineasFacturaCompra','id_factura')->get();
    }
    public function client()
    {
        return $this->belongsTo('App\Cliente','cliente')->first();
    }
}
