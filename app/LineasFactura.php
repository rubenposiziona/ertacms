<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasFactura extends Model
{
    protected $table = 'lineas_factura';

    protected $fillable = [
        'id_factura', 'id_producto', 'id_formato', 'cantidad', 'precio', 'impuesto', 'total',
    ];

	//Claves ajenas
	public function order()
    {
        return $this->belongsTo('App\Facturas','id_factura')->get();
    }
    public function product()
    {
        return $this->belongsTo('App\Producto','id_producto')->first();
    }
    public function format()
    {
        return $this->belongsTo('App\FormatosVenta','id_formato')->first();
    }
}
