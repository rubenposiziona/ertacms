<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $table = 'roles';

    public function sections()
    {
        //Final Model - Intermediate table - FK current model - FK another model
        return $this->belongsToMany('App\Section','section_role')->where("active",1);
    }

    public function users()
    {
        //Final Model - Intermediate table - FK current model - FK another model
        return $this->belongsToMany('App\User')->where("active",1);
    }
}