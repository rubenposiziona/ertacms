<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $table = 'pedidos';

	protected $fillable = [
    	'estado', 'num_pedido', 'cliente', 'fecha_pedido', 'fecha_entrega', 'archivo', 'observaciones', 'subtotal', 'iva', 'total', 'nombre', 'cif', 'nom_comercial', 'direccion', 'poblacion', 'provincia', 'c_postal', 'telefono', 'movil', 'fax', 'email', 'formaPago', 'id_presupuesto',
    ];

    protected $dates = ['fecha_pedido','fecha_entrega'];

	//Lineas de pedido
    public function lines()
    {
        return $this->hasMany('App\LineasPedido','id_pedido')->get();
    }
    public function client()
    {
        return $this->belongsTo('App\Cliente','cliente')->first();
    }
    public function state()
    {
        return $this->belongsTo('App\EstadosPedido','estado')->first();
    }
    public static function sumTotalPending()
    {
        return Pedidos::where('estado',1)->sum('total');
    }
    public static function numPending()
    {
        return Pedidos::where('estado',1)->count();
    }
    public static function sumTotalAccept()
    {
        return Pedidos::where('estado',2)->sum('total');
    }
    public static function numAccept()
    {
        return Pedidos::where('estado',2)->count();
    }
}
