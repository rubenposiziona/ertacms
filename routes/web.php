<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'HomeController@index');

//Rutas clientes
Route::get('/clientes', 'ClientesController@index');
Route::get('/clientes/ficha-cliente/{id_cliente?}', 'ClientesController@getClientSheet');
Route::post('/clientes/ficha-cliente', 'ClientesController@save');
Route::get('/clientes/eliminar/{id_cliente}', 'ClientesController@deleteClient');

//Rutas proveedores
Route::group(['middleware' => ['role:admin']], function () {
	Route::get('/proveedores', 'ProveedoresController@index');
	Route::get('/proveedores/ficha-proveedor', 'ProveedoresController@getNewSheet');
	Route::get('/proveedores/ficha-proveedor/{id_proveedor}', 'ProveedoresController@getClientSheet');
	Route::post('/proveedores/ficha-proveedor', 'ProveedoresController@save');
	Route::get('/proveedores/eliminar/{id_proveedor}', 'ProveedoresController@deleteClient');
});

//Rutas productos
Route::get('/productos', 'ProductosController@index');
Route::get('/productos/ficha-producto', 'ProductosController@getNewSheet');
Route::get('/productos/ficha-producto/{id_producto}', 'ProductosController@getProductSheet');
Route::post('/productos/ficha-producto', 'ProductosController@save');
Route::get('/productos/eliminar/{id_producto}', 'ProductosController@deleteProduct');

//Rutas compras
Route::group(['middleware' => ['role:admin']], function () {
	Route::get('/compras', 'ComprasController@index');
	Route::get('/compras/ficha-factura', 'ComprasController@getBillSheet');
	Route::get('/compras/ficha-factura/{id_factura}', 'ComprasController@getBillSheet');
	Route::post('/compras/ficha-factura', 'ComprasController@save');
});

//Rutas ventas
Route::get('/ventas', 'VentasController@index');
Route::get('/ventas/ficha-presupuesto', 'VentasController@getBudgetSheet');
Route::get('/ventas/ficha-presupuesto/{id_presupuesto}', 'VentasController@getBudgetSheet');
Route::post('/ventas/ficha-presupuesto', 'VentasController@saveBudget');
Route::get('/ventas/historico/{id_presupuesto}', 'VentasController@budgetVersions');

Route::group(['middleware' => ['role:admin']], function () {
	Route::get('/ventas/ficha-pedido', 'VentasController@getOrderSheet');
	Route::get('/ventas/ficha-pedido/{id_pedido}', 'VentasController@getOrderSheet');
	Route::post('/ventas/ficha-pedido', 'VentasController@saveOrder');
	Route::post('/ventas/generar-pedido', 'VentasController@generateOrder');
	Route::post('/ventas/generar-factura', 'VentasController@generateBill');
});

Route::post('/ajax/formatos', 'VentasController@ajaxFormats');
Route::post('/ajax/precio', 'VentasController@ajaxPrice');

//Rutas facturas
Route::group(['middleware' => ['role:admin']], function () {
	Route::get('/facturas', 'FacturasController@index');
	Route::post('/facturas/ficha-factura', 'FacturasController@saveBill');
	Route::get('/facturas/ficha-factura/{id_factura}', 'FacturasController@getBillSheet');
});

//Rutas comerciales
Route::group(['middleware' => ['role:admin']], function () {
	Route::get('/comerciales', 'ComercialesController@index');
	Route::get('/comerciales/ficha-comercial', 'ComercialesController@getNewSheet');
	Route::get('/comerciales/ficha-comercial/{id_comercial}', 'ComercialesController@getSellerSheet');
	Route::post('/comerciales/ficha-comercial', 'ComercialesController@save');
	Route::get('/comerciales/eliminar/{id_comercial}', 'ComercialesController@deleteSeller');
});

//Rutas informes
Route::group(['middleware' => ['role:admin']], function () {
	Route::get('/informes', 'InformesController@index');
});

//Rutas pdf
Route::get('/pdf', 'PdfController@index');
Route::get('/pdf/pedido/{id_pedido}', 'PdfController@generateOrder');
Route::get('/pdf/factura/{id_factura}', 'PdfController@generateInvoice');
