@extends('layouts.app')

@section('content')
<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Dashboard</h1>

            <div class="panel panel-default">
                <div class="panel-heading">Ventas</div>

                <div class="panel-body">
                    <a href="{{ url('/ventas/ficha-presupuesto') }}" class="btn btn-primary">Nuevo presupuesto</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Productos</div>

                <div class="panel-body">
                    
			        <a href="{{ url('/productos') }}" class="btn btn-danger">Ver productos</a>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Clientes</div>

                <div class="panel-body">
                    <a href="{{ url('/clientes') }}" class="btn btn-success">Ver cartera de clientes</a>
                    <a href="{{ url('/clientes/ficha-cliente') }}" class="btn btn-success">Nuevo cliente</a>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
