<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="robots" content="noindex,nofollow">

    <link rel="shortcut icon" href="{{{ asset('img/favicon.ico') }}}">

    <title>{{ config('app.name', 'CMS ERTA') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!-- Datatable -->
    <link href="{{asset('css/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatables.bootstrap.css')}}" rel="stylesheet">
    <!-- Selectpicker -->
    <link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert.css')}}">
    <!-- Loader -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/loaders.min.css')}}"/> 
    <!-- Date picker -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datepicker.min.css')}}">

    <link href="{{asset('css/mystyle.css')}}" rel="stylesheet">
    
    @stack('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color:#2b3643;margin-bottom:0;border:0;">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('img/erta.png') }}" alt="ERTA Lubrificantes" title="ERTA Lubrificantes" style="max-height: 47px;max-height: 42px;margin-top: 5px;">
                    </a>
                </div>

            </div>
        </nav>

        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav" style="margin-left:0;">
                    <li class="sidebar-brand">
                        <a href="#menu-toggle"  id="menu-toggle" > <i class="fa fa-bars " aria-hidden="true" aria-hidden="true"></i>
                    </li>
                    @foreach(Section::sectionByRole(Auth::user()->rolesUser()->id) as $seccion)
                        @if (Session::get('section') == $seccion->slug)
                            <?php $activeMenu = "active"; ?>
                        @else
                            <?php $activeMenu = ""; ?>
                        @endif
                        <li>
                            <a href="{{ url('/'.$seccion->url) }}" class="<?=$activeMenu?>">
                                <i class="fa fa-{{ $seccion->icon }}" aria-hidden="true"></i> <span>{{ $seccion->nombre }}</span>
                            </a>
                        </li>
                    @endforeach
                    <li>
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"> </i> <span style="margin-left:10px;"> Cerrar sesión</span>
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->
        </div>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                        @yield('content')

                        <!--<div class="loaders">
                            <div class="loader">
                                <div class="loader-inner ball-pulse">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>-->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{asset('js/app.js')}}"></script>
    <!-- Selectpicker -->
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <!-- Loader -->
    <script src="{{asset('js/loaders.css.js')}}"></script>
    <!-- Datepicker -->
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <!-- Sweet alert -->
    <script src="{{asset('js/sweetalert.min.js')}}"></script>


    <script type="text/javascript">
        $(window).on('load', function() {
            //Fade out loader
            $(".loaders").fadeOut("slow");;
        });
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>

    @stack('scripts')

</body>
</html>