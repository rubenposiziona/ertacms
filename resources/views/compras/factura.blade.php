@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            @if($factura->id)
                <h1>Factura nº {{ $factura->num_factura }}</h1>
            @else
                <h1>Nueva factura</h1>
            @endif

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/compras') }}">Compras</a></li>
                @if($factura->id)
                <li class="active">Factura nº {{ $factura->num_factura }}</li>
                @else
                <li class="active">Nueva factura</li>
                @endif
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        <div class="btn-toolbar pull-right">
            <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            {{ Form::open(['id' => 'nueva_factura', 'url' => 'compras/ficha-factura', 'method' => 'post']) }}
                {{ Form::hidden('id_factura', $factura->id) }}
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group pull-right">
                        <h2 class="text-right" style="margin-top:0;">TOTAL</h2>
                        <h3 class="text-right suma_total" style="margin-top:0;">{{ money_format('%.2n',$factura->total) }}</h3>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('proveedor', 'Proveedor')}}
                        {{ Form::select('proveedor', ['-1' => 'Selecciona un proveedor'] + $proveedor, $factura->id_proveedor, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('fecha_factura', 'Fecha factura')}}
                        {!! Form::text('fecha_factura', $factura->fechaFacturaFormat, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('fecha_vencimiento', 'Fecha vencimiento')}}
                        {!! Form::text('fecha_vencimiento', $factura->fechaVencimientoFormat, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <hr>

                <div id="lineas_pedido">
                    <h4>Líneas factura</h4>
                    @foreach($lineasFactura as $linea)
                    <div class="linea_p">
                        {{ Form::hidden('idLinea[]', $linea->id) }}
                        <div class="col-sm-8 col-xs-12 form-group nopadding">
                            {{ Form::label('producto', 'Producto')}}
                            {{ Form::text('producto[]', $linea->producto, ['class' => 'form-control nobradius']) }}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding qty">
                            {{ Form::label('cantidad', 'Cantidad')}}
                            {!! Form::text('cantidad[]', $linea->cantidad, ['class' => 'form-control nobradius text-right inputQty']) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding prize">
                            {{ Form::label('precio', 'Precio')}}
                            {!! Form::text('precio[]', number_format($linea->precio, 2, '.', ''), ['class' => 'form-control nobradius text-right inputPrize']) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding iva">
                            {{ Form::label('impuesto', 'Imp. (%)')}}
                            {!! Form::text('impuesto[]', number_format($linea->impuesto, 2, '.', ''), ['class' => 'form-control nobradius text-right inputIva']) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding totalLine">
                            {{ Form::label('total', 'Total')}}<br>
                            {!! Form::text('total[]', number_format($linea->total, 2, '.', ''), ['class' => 'form-control nobradius text-right inputTotal', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach
                </div>

                <div class="col-xs-12 nopadding">
                    {!! Form::button('<i class="fa fa-plus"></i> Añadir nueva línea', ['class' => 'btn btn-danger', 'onclick' => 'addLine(); return false;'] )  !!}
                </div>
                <div class="clearfix"></div>
                <hr>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_subtotal"><?=money_format('%.2n',$factura->subtotal)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>SUBTOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_iva"><?=money_format('%.2n',$factura->iva)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>IVA</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_total"><?=money_format('%.2n',$factura->total)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>TOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <hr>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {{ Form::label('observaciones', 'Observaciones')}}
                        {!! Form::textarea('observaciones', $factura->observaciones, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <h4>Adjuntar archivo</h4>
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group">
                        {{ Form::label('titulo_arch', 'Título')}}
                        {!! Form::text('titulo_arch','', ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group">
                        {{ Form::label('archivo', 'Archivo')}}
                        {!! Form::file('archivo') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            {{ Form::close() }}

        </div>

        <div class="btn-toolbar pull-right">
            <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $( "#fecha_factura" ).datepicker({autoclose:true});
        $( "#fecha_vencimiento" ).datepicker({autoclose:true});

        $(".btn_save").on('click', function(e){
            e.preventDefault();
            $('#nueva_factura').submit();
        });

       loadOperations();
    });

    function addLine() {

        var content = '<div class="linea_p">{{ Form::hidden("idLinea[]", "") }}<div class="col-sm-8 col-xs-12 form-group nopadding">{{ Form::label("producto", "Producto")}}{{ Form::text("producto[]", "", ["class" => "form-control nobradius productoSelect"]) }}</div><div class="col-sm-1 col-xs-12 form-group nopadding qty">{{ Form::label("cantidad", "Cantidad")}}{!! Form::text("cantidad[]", "", ["class" => "form-control nobradius text-right inputQty"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding prize">{{ Form::label("precio", "Precio")}}{!! Form::text("precio[]", number_format(0, 2, ".", ""), ["class" => "form-control nobradius text-right inputPrize"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding iva">{{ Form::label("impuesto", "Imp. (%)")}}{!! Form::text("impuesto[]", number_format(21, 2, ".", ""), ["class" => "form-control nobradius text-right inputIva"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding totalLine">{{ Form::label("total", "Total")}}<br>{!! Form::text("total[]", number_format(0, 2, ".", ""), ["class" => "form-control nobradius text-right inputTotal", "readonly" => "readonly"]) !!}</div><div class="clearfix"></div></div>';

            $('#lineas_pedido').append(content);
            loadOperations();
            return false;
    }

    function loadOperations() {
        $('.inputQty').on('change',function(){
            var qty     = $(this).val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();

            calculateTotalLine(qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputPrize').on('change',function(){
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();
            var precio  = $(this).val();

            calculateTotalLine(qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputIva').on('change',function(){
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).val();

            calculateTotalLine(qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.productoSelect').on('change',function(){
            var $select = $(this).parent().siblings('.formatDiv').find('select');
            if($(this).val() > 0){
                $.ajax({
                    url: "{{ url('/ajax/formatos') }}",
                    data: { enviado:1,idProducto:$(this).val(),"_token":"{{ csrf_token() }}" },
                    type: "post",
                    success: function(data){
                        $select.html(data.html);
                    }
                });
            }else{
                $(this).parent().siblings('.formatDiv').find('select').empty();
            }
        });
    }

    function calculateTotalLine(qty,prize,iva,element){
        var total = parseFloat(qty * prize * (1+iva/100)).toFixed(2);
        element.val(total);

        calculateTotals();
    }
    
    function calculateTotals() {
        var suma_subtotal   = 0.00;
        var iva_total       = 0.00;
        var suma_total      = 0.00;
        $('.inputTotal').each(function() {
            var precioLinea = $(this).parent().siblings('.prize').find('input').val();
            var ivaLinea    = $(this).parent().siblings('.iva').find('input').val();
            var qtyLinea    = $(this).parent().siblings('.qty').find('input').val();

            suma_subtotal   += precioLinea * qtyLinea;
            iva_total       += precioLinea * qtyLinea * (ivaLinea/100);
            suma_total      += precioLinea * qtyLinea * (1 + (ivaLinea/100));
        });
        $('.suma_subtotal').html(parseFloat(suma_subtotal).toFixed(2) + ' Eu');
        $('.suma_iva').html(parseFloat(iva_total).toFixed(2) + ' Eu');
        $('.suma_total').html(parseFloat(suma_total).toFixed(2) + ' Eu');
    }
</script>
@endpush