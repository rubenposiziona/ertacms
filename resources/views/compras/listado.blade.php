@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Compras</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Listado compras</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/compras/ficha-factura') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva factura</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="compras-table">
                    <thead>
                        <tr>
                            <th class="text-center">Referencia</th>
                            <th class="text-center">Descripción</th>
                            <th class="text-center">Formato</th>
                            <th class="text-center">Precio unitario</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                		@foreach($facturas as $factura)
                            <tr>
                                <td class="text-center">{{ $factura->referencia }}</td>
                                <td>{{ $factura->descripcion }}</td>
                                <td class="text-center">{{ $factura->formato }}</td>
                                <td class="text-right">{{ money_format('%.2n',$factura->precio) }}</td>
                                <td class="text-center"><a href="{{ url('/compras/ficha-factura/'.Crypt::encrypt($factura->id)) }}"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('#compras-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                select: true,
            });
        });
    </script>
@endpush