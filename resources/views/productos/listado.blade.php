@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Productos</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Listado productos</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        @permission('create-product')
        <div class="btn-toolbar pull-right">
            <a href="{{ url('/productos/ficha-producto') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo producto</a>
        </div>
        <div class="clearfix"></div>
        @endpermission

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="productos-table">
                    <thead>
                        <tr>
                            <th class="text-center">Referencia</th>
                            <th class="text-center">Descripción</th>
                            <th class="text-center">Formato</th>
                            <th class="text-center">Precio unitario</th>
                            <th></th>
                            @permission('delete-product')
                            <!--<th></th>-->
                            @endpermission
                        </tr>
                    </thead>
                    <tbody>
                    		@foreach($productos as $producto)
                                <tr>
                                    <td class="text-center">{{ $producto->referencia }}</td>
                                    <td>{{ $producto->descripcion }}</td>
                                    <td class="text-center">{{ $producto->formato }}</td>
                                    <td class="text-right">{{ money_format('%.2n',$producto->precio) }}</td>
                                    <td class="text-center"><a href="{{ url('/productos/ficha-producto/'.Crypt::encrypt($producto->id)) }}"><i class="fa fa-pencil"></i></a></td>
                                    @permission('delete-product')
                                    <!--<td class="text-center"><a href="#" onclick="deleteProduct({{ $producto->id }}); return false;"><i class="fa fa-close"></i></a></td>-->
                                    @endpermission
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#productos-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
    });

    @permission('delete-product')
    function deleteProduct(id_product) {
        swal({
            title: "¿Está seguro de eliminar el producto?",
            type: "error",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Eliminar!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
        function(){
            window.location.href="{{ url('/productos/eliminar') }}" + "/" + id_product;
        });
    }
    @endpermission

</script>
@endpush