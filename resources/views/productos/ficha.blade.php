@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Ficha producto</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/productos') }}">Productos</a></li>
                <li class="active">Ficha producto</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        @permission('update-product,create-product')
        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>
        @endpermission

        <div class="col-xs-12">

            @include('includes.errors')

            {{ Form::open(['id' => 'nuevo_producto', 'url' => 'productos/ficha-producto', 'method' => 'post']) }}
                {{ Form::hidden('id_producto', $producto->id) }}

                <div class="row">
                    <div id="lineas_prod">
                        <div class="col-sm-6 col-xs-12 form-group">
                            {{ Form::label('referencia', 'Referencia')}}
                            {!! Form::text('referencia', $producto->referencia, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-sm-3 col-xs-12 form-group">
                            {{ Form::label('familia', 'Familia')}}
                            {{ Form::select('familia', $subfamilia, $producto->id_subfamilia, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) }}
                        </div>
                        <div class="col-sm-3 col-xs-12 form-group">
                            {{ Form::label('composicion', 'Composición')}}
                            {{ Form::select('composicion', $composicion, $producto->id_composicion, ['class' => 'form-control selectpicker']) }}
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 form-group">
                            {{ Form::label('descripcion', 'Descripción')}}
                            {!! Form::textarea('descripcion', $producto->descripcion, ['class' => 'form-control', 'rows' => '1']) !!}
                        </div>
                        <div class="clearfix"></div>
                        @foreach($formatos as $formato)
                            <div class="col-sm-3 col-xs-12 form-group">
                                {{ Form::label('formato', 'Formato')}}
                                {!! Form::text('formato['.$formato->id.']', $formato->referencia, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                            </div>
                            <div class="col-sm-3 col-xs-12 form-group">
                                {{ Form::label('precio_'.$formato->id, 'Precio')}}
                                {!! Form::text('precio_'.$formato->id, $formato->precio, ['class' => 'form-control']) !!}
                            </div>
                            <div class="clearfix"></div>
                        @endforeach
                    </div>
                </div>

            {{ Form::close() }}

        </div>

        @permission('update-product,create-product')
        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>
        @endpermission

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {

        $(".btn_save").on('click', function(){
            $('#nuevo_producto').submit();
        });

    });



</script>
@endpush