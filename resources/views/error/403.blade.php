@extends('layouts.app')

@section('content')
<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Error 403: Acceso restringido</h1>
            <p>La sección actual tiene el acceso restringido.</p>

        </div>
    </div>
</div>
@endsection
