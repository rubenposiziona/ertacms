@extends('layouts.app')

@section('content')
<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Proveedores</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Listado proveedores</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/proveedores/ficha-proveedor') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo proveedor</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @include('includes.errors')

            <table class="table table-hover table-bordered" id="proveedores-table">
                <thead>
                    <tr>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">NIF</th>
                        <th class="text-center">Población</th>
                        <th class="text-center">Teléfono</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
            		@foreach($proveedores as $proveedor)
                        <tr>
                            <td>{{ $proveedor->nombre }}</td>
                            <td class="text-center">{{ $proveedor->nif }}</td>
                            <td class="text-center">{{ $proveedor->poblacion }}</td>
                            <td class="text-center">{{ $proveedor->telefono }}</td>
                            <td class="text-center"><a href="{{ url('/proveedores/ficha-proveedor/'.Crypt::encrypt($proveedor->id)) }}"><i class="fa fa-pencil"></i></a></td>
                            <td class="text-center"><a href="#" onclick="deleteUser({{ $proveedor->id }}); return false;"><i class="fa fa-close"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#proveedores-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
    });

    function deleteUser(id_proveedor) {

        swal({
            title: "¿Está seguro de eliminar el proveedor?",
            type: "error",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Eliminar!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
        function(){
            window.location.href="{{ url('/proveedores/eliminar') }}" + "/" + id_proveedor;
        });

    }

</script>
@endpush