@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Ficha proveedor</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/proveedores') }}">Proveedores</a></li>
                <li class="active">Ficha proveedor</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @include('includes.errors')

            {{ Form::open(['id' => 'nuevo_proveedor', 'url' => 'proveedores/ficha-proveedor', 'method' => 'post']) }}
                {{ Form::hidden('id_proveedor', $proveedor->id) }}
                <div class="row">

                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('nombre', 'Nombre completo')}}
                        {!! Form::text('nombre', $proveedor->nombre, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('nif', 'NIF / CIF')}}
                        {!! Form::text('nif', $proveedor->nif, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('email', 'Email')}}
                        {!! Form::text('email', $proveedor->email, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 form-group">
                        {{ Form::label('direccion', 'Dirección')}}
                        {!! Form::text('direccion', $proveedor->direccion, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('codigo_postal', 'Código postal')}}
                        {!! Form::text('codigo_postal', $proveedor->c_postal, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('poblacion', 'Población')}}
                        {!! Form::text('poblacion', $proveedor->poblacion, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('provincia', 'Provincia')}}
                        {!! Form::text('provincia', $proveedor->provincia, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group">
                        {{ Form::label('pais', 'País')}}
                        {{ Form::select('pais', ['-1' => 'Selecciona un país'] + $paises, $proveedor->pais, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('telefono', 'Teléfono')}}
                        {!! Form::text('telefono', $proveedor->telefono, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('movil', 'Móvil')}}
                        {!! Form::text('movil', $proveedor->movil, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>

                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {{ Form::label('observaciones', 'Observaciones')}}
                        {!! Form::textarea('observaciones', $proveedor->observaciones, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <h4>Adjuntar archivo</h4>
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group">
                        {{ Form::label('titulo_arch', 'Título')}}
                        {!! Form::text('titulo_arch','', ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group">
                        {{ Form::label('archivo', 'Archivo')}}
                        {!! Form::file('archivo') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            {{ Form::close() }}

        </div>

        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        
        $(".btn_save").on('click', function(){
            $('#nuevo_proveedor').submit();
        });

    });


</script>
@endpush