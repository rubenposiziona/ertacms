<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Albarán </title>
    <style type="text/css">

      body {
        font-size:12px;
      }

      header {
        text-align: center;
      }

      table tr td {
        padding: 4px;
      }

      #header-table {
        border-collapse:collapse;
        border-top:0;
        border-right:0;
        margin-top:20px;
      }

      #header-table tr td{
        padding:5px;
      }

      #client-table {
        border-collapse:collapse;
      }

      #client-table tbody tr:first-child td {
        background-color: #00457C; 
        color: #fff; 
        border-color: #000; 
        padding: 5px;
      }

      #product-table {
        border-collapse:collapse;
        border-left:0;
        border-bottom:0;
      }

      #product-table tbody tr:first-child td {
        background-color: #00457C; 
        color: #fff; 
        border-color: #000;
        text-align: center;
      }

      .info-order p {
        margin: 0;
      }

      footer {
        position:absolute;
        bottom:0;
      }

      footer .info {
        text-align:center;
      }

      footer .info p {
        margin-top:0; 
        margin-bottom:0;
      }

    </style>
  </head>
  <body>
    <div class="page-break">
      <header>
        <img src="img/erta_color.jpg">
      </header>
      <table id="header-table" border="1">
        <tbody>
          <tr>
            <td>Fecha pedido</td>
            <td> 27/01/2017</td>
          </tr>
          <tr>
            <td>Albarán Nº</td>
            <td>{{ $pedido->num_pedido }}</td>
          </tr>
        </tbody>
      </table>
      <table id="client-table" border="1" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td colspan="6"><strong>DIRECCIÓN DE FACTURACIÓN</strong></td>
          </tr>
          <tr>
            <td>Nombre</td>
            <td colspan="3">{{ $pedido->nombre }}</td>
            <td>CIF / VAT</td>
            <td>{{ $pedido->cif }}</td>
          </tr>
          <tr>
            <td>Nombre comercial</td>
            <td colspan="5">{{ $pedido->nom_comercial }}</td>
          </tr>
          <tr>
            <td>Dirección</td>
            <td colspan="5">{{ $pedido->direccion }}</td>
          </tr>
          <tr>
            <td>Población</td>
            <td>{{ $pedido->poblacion }}</td>
            <td>Provincia</td>
            <td>{{ $pedido->provincia }}</td>
            <td>C.P.</td>
            <td>{{ $pedido->c_postal }}</td>
          </tr>
          <tr>
            <td>Teléfono</td>
            <td>{{ $pedido->telefono }}</td>
            <td>Telef. móvil</td>
            <td>{{ $pedido->movil }}</td>
            <td>Fax</td>
            <td>{{ $pedido->fax }}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td colspan="5">{{ $pedido->email }}</td>
          </tr>
          <tr>
            <td>Fecha entrega</td>
            <td colspan="1">@if($pedido->fecha_entrega) {{ $pedido->fecha_entrega->format('d/m/Y') }} @endif</td>
            <td>Horario de entrega</td>
            <td colspan="3"></td>
          </tr>
          <tr>
            <td>Persona que hace el pedido</td>
            <td colspan="5"></td>
          </tr>
          <tr>
            <td>Cargo</td>
            <td colspan="1"></td>
            <td>Dpto.</td>
            <td colspan="3"></td>
          </tr>
          <tr>
            <td colspan="6" style="background-color: #00457C;color: #fff;border-color: #000;padding: 5px;"><strong>DIRECCIÓN DE ENTREGA</strong> (si es distinta a la de facturación)</td>
          </tr>
          <tr>
            <td>Dirección</td>
            <td colspan="5"></td>
          </tr>
          <tr>
            <td>Población</td>
            <td></td>
            <td>Provincia</td>
            <td></td>
            <td>C.P.</td>
            <td></td>
          </tr>
          <tr>
            <td>Persona que hace el pedido</td>
            <td colspan="5"></td>
          </tr>
          <tr>
            <td>Horario de entrega</td>
            <td colspan="5"></td>
          </tr>
        </tbody>
      </table>

      <br><br>

      <table id="product-table" border="1" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td><strong>Referencia</strong></td>
            <td><strong>Descripcion</strong></td>
            <td><strong>Cantidad</strong></td>
            <td><strong>Formato</strong></td>
            <td><strong>Importe ud</strong></td>
            <td><strong>Total</strong></td>
          </tr>
          @foreach($lineas_pedido as $linea)
            <tr>
              <td>{{ $linea->product()->referencia }}</td>
              <td>{{ $linea->product()->descripcion }}</td>
              <td style="text-align:center;">{{ $linea->cantidad }}</td>
              <td style="text-align:center;">{{ $linea->format()->referencia }}</td>
              <td style="text-align:right;"></td>
              <td style="text-align:right;"></td>
            </tr>
          @endforeach
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Importe neto</strong></td>
            <td style="text-align:right;font-size:13px;"><strong></strong></td>
          </tr>
          <tr>
            <td style="border:0;"></td>
            <td style="text-align:center;background-color: #00457C;color: #fff;border-color: #000;">AGENTE DE ZONA QUE REALIZA EL PEDIDO</td>
            <td colspan="2" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Gtos envío</strong></td>
            <td style="text-align:right;font-size:13px;"><strong></strong></td>
          </tr>
          <tr>
            <td style="border:0;"></td>
            <td style="border-bottom:0;"></td>
            <td colspan="2" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Total</strong></td>
            <td style="text-align:right;font-size:13px;"><strong></strong></td>
          </tr>
          <tr>
            <td style="border:0;"></td>
            <td style="border-top:0;"></td>
            <td colspan="2" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>21% IVA</strong></td>
            <td style="text-align:right;font-size:13px;"><strong></strong></td>
          </tr>
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Total IVA incl.</strong></td>
            <td style="text-align:right;font-size:13px;"><strong></strong></td>
          </tr>
        </tbody>
      </table>

      <div class="info-order">
        <p><strong>PAGOS</strong></p>
        <p>Solo se admitirán pagos mediante sistema SEPA B2B o transferencias</p>
        <p><strong>CALIDAD</strong></p>
        <p>Los clientes podrán solicitar una muestra de producto previa a compra</p>
        <p><strong>PORTES</strong></p>
        <p>El transporte de la mercancía va incluido en el precio final del producto</p>
      </div>

      <footer>
        <hr>
        <!--<p><strong>El pago de esta factura, sólo se acreditará con el adeudo bancario o recibí de caja.</strong></p>
        <p><strong>DATOS BANCARIOS: IBAN ES</strong></p>-->
        <div class="info">
          <p>PUEDE PONERSE EN CONTACTO CON LA COMPAÑÍA MEDIANTE</p>
          <p>www.erta.es</p>
          <p>Erta comercializa exclusivamente productos certificados</p>
          <p>Si desea información relativa a cualquier producto, no dude en ponerse en contacto con nosotros.</p>
        </div>
      </footer>
    </div></body></html>