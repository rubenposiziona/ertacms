<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Factura</title>
    <style type="text/css">

      body {
        font-size:12px;
      }

      header {
        text-align: center;
      }

      table tr td {
        padding: 4px;
      }

      #header-table {
        border-collapse:collapse;
        border-top:0;
        border-right:0;
        margin-top:20px;
      }

      #header-table tr td{
        padding:5px;
      }

      #client-table {
        border-collapse:collapse;
      }

      #client-table tbody tr:first-child td {
        background-color: #00457C; 
        color: #fff; 
        border-color: #000; 
        padding: 5px;
      }

      #product-table {
        border-left:0;
        border-bottom:0;
         border-collapse:collapse;
      }

      #product-table tbody tr:first-child td {
        background-color: #00457C; 
        color: #fff; 
        border-color: #000;
        text-align: center;
      }

      footer {
        position:absolute;
        bottom:0;
      }

      footer .info {
        text-align:center;
      }

      footer .info p {
        margin-top:0; 
        margin-bottom:0;
      }

    </style>
  </head>
  <body>
    <div class="page-break">
      <header>
        <img src="img/erta_color.jpg">
      </header>
      <table id="header-table" border="1">
        <tbody>
          <tr>
            <td>Fecha factura</td>
            <td>{{ date("d/m/Y", strtotime($factura->fecha_factura)) }}</td>
            <td style="border:0;"></td>
            <td style="border:0;"></td>
          </tr>
          <tr>
            <td>Procede de Albarán Nº</td>
            <td>{{ $factura->id_pedido }}</td>
            <td>Factura Nº</td>
            <td>{{ $factura->num_factura }}</td>
          </tr>
        </tbody>
      </table>
      <table id="client-table" border="1" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td colspan="6"><strong>DATOS CLIENTE</strong></td>
          </tr>
          <tr>
            <td>Nombre</td>
            <td colspan="3">{{ $factura->nombre }}</td>
            <td>CIF / VAT</td>
            <td>{{ $factura->cif }}</td>
          </tr>
          <tr>
            <td>Nombre comercial</td>
            <td colspan="5">{{ $factura->nom_comercial }}</td>
          </tr>
          <tr>
            <td>Dirección</td>
            <td colspan="5">{{ $factura->direccion }}</td>
          </tr>
          <tr>
            <td>Población</td>
            <td>{{ $factura->poblacion }}</td>
            <td>Provincia</td>
            <td>{{ $factura->provincia }}</td>
            <td>C.P.</td>
            <td>{{ $factura->c_postal }}</td>
          </tr>
          <tr>
            <td>Teléfono</td>
            <td>{{ $factura->telefono }}</td>
            <td>Telef. móvil</td>
            <td>{{ $factura->movil }}</td>
            <td>Fax</td>
            <td>{{ $factura->fax }}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td colspan="5">{{ $factura->email }}</td>
          </tr>
        </tbody>
      </table>

      <br><br>

      <table id="product-table" border="1" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td><strong>Referencia</strong></td>
            <td><strong>Descripcion</strong></td>
            <td><strong>Cantidad</strong></td>
            <td><strong>Formato</strong></td>
            <td><strong>Importe ud</strong></td>
            <td><strong>Total</strong></td>
          </tr>
          @foreach($lineas_factura as $linea)
            <tr>
              <td>{{ $linea->product()->referencia }}</td>
              <td>{{ $linea->product()->descripcion }}</td>
              <td style="text-align:center;">{{ $linea->cantidad }}</td>
              <td style="text-align:center;">{{ $linea->format()->referencia }}</td>
              <td style="text-align:right;">{{ $linea->precio }}</td>
              <td style="text-align:right;">{{ $linea->total }} €</td>
            </tr>
          @endforeach
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Importe neto</strong></td>
            <td style="text-align:right;font-size:13px;"><strong>{{ $factura->subtotal }} €</strong></td>
          </tr>
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Gtos envío</strong></td>
            <td style="text-align:right;font-size:13px;"><strong></strong></td>
          </tr>
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Total</strong></td>
            <td style="text-align:right;font-size:13px;"><strong>{{ $factura->subtotal }} €</strong></td>
          </tr>
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>21% IVA</strong></td>
            <td style="text-align:right;font-size:13px;"><strong>{{ $factura->iva }} €</strong></td>
          </tr>
          <tr>
            <td colspan="4" style="border:0;"></td>
            <td style="text-align:center;font-size:13px;"><strong>Total IVA incl.</strong></td>
            <td style="text-align:right;font-size:13px;"><strong>{{ $factura->total }} €</strong></td>
          </tr>
        </tbody>
      </table>

      @if($factura->formaPago && $factura->formaPago!='-1')
      <table id="payment-table" border="0">
        <tbody>
          <tr>
            <td><strong>FORMA DE PAGO:</strong></td>
            <td>{{ $factura->payment()->tipo }}</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
      </table>
      @endif
      {{-- <p><strong>VENCIMIENTOS</strong></p> --}}
      

      <footer>
        <hr>
        <p><strong>El pago de esta factura, sólo se acreditará con el adeudo bancario o recibí de caja.</strong></p>
        <p><strong>DATOS BANCARIOS: IBAN ES</strong></p>
        <div class="info">
          <p>www.erta.es</p>
          <p>Erta comercializa exclusivamente productos certificados</p>
          <p>Si desea información relativa a cualquier producto, no dude en ponerse en contacto con nosotros.</p>
          <p>Grupo Erta Lubricantes, CIF: B54892807 ; Ancha de Castelar 34, San Vicente del Raspeig, España</p>
        </div>
      </footer>
    </div></body></html>