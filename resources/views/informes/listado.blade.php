@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Informes</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Informes</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <br><br>
        <br><br>
        <br><br>
        <div id="chart_stock"></div>
        <?=$lava->render('ColumnChart', 'Total', 'chart_stock') ?>

    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {

    });

</script>
@endpush