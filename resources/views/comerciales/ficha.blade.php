@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Ficha comercial</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/comerciales') }}">Comerciales</a></li>
                <li class="active">Ficha comercial</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @include('includes.errors')

            {{ Form::open(['id' => 'nuevo_comercial', 'url' => 'comerciales/ficha-comercial', 'method' => 'post']) }}
                {{ Form::hidden('id_comercial', $comercial->id) }}
                <div class="row">

                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('nombre', 'Nombre completo')}}
                        {!! Form::text('nombre', $comercial->nombre, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('nif', 'NIF / CIF')}}
                        {!! Form::text('nif', $comercial->nif, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('email', 'Email')}}
                        {!! Form::text('email', $comercial->email, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 form-group">
                        {{ Form::label('direccion', 'Dirección')}}
                        {!! Form::text('direccion', $comercial->direccion, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('codigo_postal', 'Código postal')}}
                        {!! Form::text('codigo_postal', $comercial->c_postal, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('poblacion', 'Población')}}
                        {!! Form::text('poblacion', $comercial->poblacion, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('provincia', 'Provincia')}}
                        {!! Form::text('provincia', $comercial->provincia, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('telefono', 'Teléfono')}}
                        {!! Form::text('telefono', $comercial->telefono, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('movil', 'Móvil')}}
                        {!! Form::text('movil', $comercial->movil, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>

                </div>

            {{ Form::close() }}

        </div>

        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        
        $(".btn_save").on('click', function(){
            $('#nuevo_comercial').submit();
        });

    });


</script>
@endpush