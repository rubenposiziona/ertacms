@extends('layouts.app')

@section('content')
<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Comerciales</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Listado comerciales</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/comerciales/ficha-comercial') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo comercial</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="comerciales-table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>NIF</th>
                            <th>Población</th>
                            <th>Teléfono</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    		@foreach($comerciales as $comercial)
                                <tr>
                                    <td>{{ $comercial->nombre }}</td>
                                    <td class="text-center">{{ $comercial->nif }}</td>
                                    <td class="text-center">{{ $comercial->poblacion }}</td>
                                    <td class="text-center">{{ $comercial->telefono }}</td>
                                    <td class="text-center"><a href="{{ url('/comerciales/ficha-comercial/'.Crypt::encrypt($comercial->id)) }}"><i class="fa fa-pencil"></i></a></td>
                                    <td class="text-center"><a href="#" onclick="deleteUser({{ $comercial->id }}); return false;"><i class="fa fa-close"></i></a></td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('#comerciales-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                select: true,
            });
        });

        function deleteUser(id_comercial) {
            swal({
                title: "¿Está seguro de eliminar el comercial?",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Eliminar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function(){
                window.location.href="{{ url('/comerciales/eliminar') }}" + "/" + id_comercial;
            });
        }
    </script>
@endpush