@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>
<?php   $disabled = []; 
        if ($presupuesto->estado != 1 && $presupuesto->estado != NULL) { $disabled = ['disabled' => 'disabled']; } 
?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            @if($presupuesto->id)
                <h1>Presupuesto nº {{ $presupuesto->num_presupuesto }} <small>(v.{{ $presupuesto->version }})</small></h1>
            @else
                <h1>Nuevo presupuesto</h1>
            @endif

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/ventas') }}">Ventas</a></li>
                @if($presupuesto->id)
                <li class="active">Presupuesto nº {{ $presupuesto->num_presupuesto }} <small>(v.{{ $presupuesto->version }})</small></li>
                @else
                <li class="active">Nuevo presupuesto</li>
                @endif
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        @if($presupuesto->estado == 1)
            <div class="btn-toolbar pull-right">
                <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
                @if($presupuesto->id)
                    <a href="#" class="btn btn-info btn_newV"><i class="fa fa-code-fork"></i> Nueva versión</a>
                    <a href="{{ url('/ventas/historico/'.Crypt::encrypt($presupuesto->id)) }}" class="btn btn-warning"><i class="fa fa-code-fork"></i> Histórico versiones</a>
                @endif
            </div>
        @elseif($presupuesto->estado == 2)
            {{ Form::open(['url' => 'ventas/generar-pedido', 'method' => 'post']) }}
                {{ Form::hidden('id_presupuesto', $presupuesto->id) }}
                <div class="btn-toolbar pull-right">
                    @if(!$presupuesto->exportado)
                    <button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Generar pedido</button>
                    @endif
                    <a href="{{ url('/ventas/historico/'.Crypt::encrypt($presupuesto->id)) }}" class="btn btn-warning"><i class="fa fa-code-fork"></i> Histórico versiones</a>
                </div>
            {{ Form::close() }}
        @endif
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @if($presupuesto->estado == 1)
            {{ Form::open(['id' => 'nuevo_presupuesto', 'url' => 'ventas/ficha-presupuesto', 'method' => 'post']) }}
                {{ Form::hidden('id_presupuesto', $presupuesto->id) }}
                {{ Form::hidden('version', $presupuesto->version, ['id' => 'version']) }}
                {{ Form::hidden('id_comercial', $presupuesto->id_comercial, ['id' => 'id_comercial']) }}
            @endif
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group pull-right">
                        <h2 class="text-right" style="margin-top:0;">TOTAL</h2>
                        <h3 class="text-right suma_total" style="margin-top:0;">{{ money_format('%.2n',$presupuesto->total) }}</h3>
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group pull-right">
                        {{ Form::label('estado', 'Estado del presupuesto')}}
                        {{ Form::select('estado', $estados, $presupuesto->estado, ['class' => 'form-control selectpicker']+$disabled) }}
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('cliente', 'Cliente')}}
                        {{ Form::select('cliente', ['-1' => 'Selecciona un cliente'] + $clientes, $presupuesto->cliente, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']+$disabled) }}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('fecha', 'Fecha')}}
                        {!! Form::text('fecha', $presupuesto->fechaPresupuestoFormat, ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('valido_hasta', 'Válido hasta')}}
                        {!! Form::text('valido_hasta', $presupuesto->fechaValidoFormat, ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <hr>
                <div id="lineas_pedido">
                    <h4>Líneas presupuesto</h4>
                    @foreach($lineasPresupuesto as $linea)
                    <div class="linea_p">
                        {{ Form::hidden('idLinea[]', $linea->id) }}
                        <div class="col-sm-6 col-xs-12 form-group nopadding productoDiv">
                            {{ Form::label('producto', 'Producto')}}
                            {{ Form::select('producto[]', ['-1' => 'Selecciona un producto'] + $productos, $linea->id_producto, ['class' => 'form-control nobradius productoSelect selectpicker', 'data-live-search' => 'true']+$disabled) }}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding formatDiv">
                            {{ Form::label('formato', 'Formato')}}
                            {{ Form::select('formato[]', $linea->formatos, $linea->id_formato, ['class' => 'form-control nobradius formatoSelect selectpicker']+$disabled) }}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding desc">
                            {{ Form::label('descuento', 'Desc. (%)')}}
                            {!! Form::text('descuento[]', $linea->descuento, ['class' => 'form-control nobradius text-right inputDesc']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding qty">
                            {{ Form::label('cantidad', 'Cantidad')}}
                            {!! Form::text('cantidad[]', $linea->cantidad, ['class' => 'form-control nobradius text-right inputQty']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding prize">
                            {{ Form::label('precio', 'Precio')}}
                            {!! Form::text('precio[]', number_format($linea->precio, 2, '.', ''), ['class' => 'form-control nobradius text-right inputPrize']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding iva">
                            {{ Form::label('impuesto', 'Imp. (%)')}}
                            {!! Form::text('impuesto[]', number_format($linea->impuesto, 2, '.', ''), ['class' => 'form-control nobradius text-right inputIva']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding totalLine">
                            {{ Form::label('total', 'Total')}}<br>
                            {!! Form::text('total[]', number_format($linea->total, 2, '.', ''), ['class' => 'form-control nobradius text-right inputTotal', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach
                </div>

                @if($presupuesto->estado == 1)
                <div class="col-xs-12 nopadding">
                    {!! Form::button('<i class="fa fa-plus"></i> Añadir nueva línea', ['class' => 'btn btn-danger', 'onclick' => 'addLine(); return false;'] )  !!}
                </div>
                <div class="clearfix"></div>
                @endif
                <hr>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_subtotal"><?=money_format('%.2n',$presupuesto->subtotal)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>SUBTOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_iva"><?=money_format('%.2n',$presupuesto->iva)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>IVA</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_total"><?=money_format('%.2n',$presupuesto->total)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>TOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <hr>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {{ Form::label('observaciones', 'Observaciones')}}
                        {!! Form::textarea('observaciones', $presupuesto->observaciones, ['class' => 'form-control', 'rows' => '2']+$disabled) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            @if($presupuesto->estado == 1)
            {{ Form::close() }}
            @endif

        </div>

        @if($presupuesto->estado == 1)
        <div class="btn-toolbar pull-right">
            <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
            @if($presupuesto->id)
                <a href="#" class="btn btn-info btn_newV"><i class="fa fa-code-fork"></i> Nueva versión</a>
                <a href="#" class="btn btn-warning"><i class="fa fa-code-fork"></i> Histórico versiones</a>
            @endif
        </div>
        @elseif($presupuesto->estado == 2)
            {{ Form::open(['url' => 'ventas/generar-pedido', 'method' => 'post']) }}
                {{ Form::hidden('id_presupuesto', $presupuesto->id) }}
                <div class="btn-toolbar pull-right">
                    @if(!$presupuesto->exportado)
                    <button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Generar pedido</button>
                    @endif
                    <a href="#" class="btn btn-warning"><i class="fa fa-code-fork"></i> Histórico versiones</a>
                </div>
            {{ Form::close() }}
        @endif
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $( "#fecha" ).datepicker({autoclose:true});
        $( "#valido_hasta" ).datepicker({autoclose:true});

        $(".btn_save").on('click', function(e){
            e.preventDefault();
            $('#nuevo_presupuesto').submit();
        });

        $(".btn_newV").on('click', function(e){
            e.preventDefault();
            $("#version").val({{ $presupuesto->version + 1 }});
            $('#nuevo_presupuesto').submit();
        });

       loadOperations();
    });

    function addLine() {

        var content = '<div class="linea_p">{{ Form::hidden("idLinea[]", "") }}<div class="col-sm-6 col-xs-12 form-group nopadding productoDiv">{{ Form::label("producto", "Producto")}}{{ Form::select("producto[]", ["-1" => "Selecciona un producto"] + $productos, "", ["class" => "form-control nobradius productoSelect selectpicker", "data-live-search" => "true"]) }}</div><div class="col-sm-1 col-xs-12 form-group nopadding formatDiv">{{ Form::label("formato", "Formato")}}{{ Form::select("formato[]", array(), "", ["class" => "form-control nobradius formatoSelect selectpicker"]) }}</div><div class="col-sm-1 col-xs-12 form-group nopadding desc">{{ Form::label("descuento", "Desc. (%)")}}{!! Form::text("descuento[]", $linea->descuento, ["class" => "form-control nobradius text-right inputDesc"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding qty">{{ Form::label("cantidad", "Cantidad")}}{!! Form::text("cantidad[]", "", ["class" => "form-control nobradius text-right inputQty"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding prize">{{ Form::label("precio", "Precio")}}{!! Form::text("precio[]", number_format(0, 2, ".", ""), ["class" => "form-control nobradius text-right inputPrize"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding iva">{{ Form::label("impuesto", "Imp. (%)")}}{!! Form::text("impuesto[]", number_format(21, 2, ".", ""), ["class" => "form-control nobradius text-right inputIva"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding totalLine">{{ Form::label("total", "Total")}}<br>{!! Form::text("total[]", number_format(0, 2, ".", ""), ["class" => "form-control nobradius text-right inputTotal", "readonly" => "readonly"]) !!}</div><div class="clearfix"></div></div>';

            $('#lineas_pedido').append(content);
            loadOperations();
            $('.selectpicker').selectpicker('refresh');
            return false;
    }

    function loadOperations() {
        $('.inputQty').on('change',function(){
            var desc    = $(this).parent().siblings('.desc').find('input').val();
            var qty     = $(this).val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();

            calculateTotalLine(desc,qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputPrize').on('change',function(){
            var desc    = $(this).parent().siblings('.desc').find('input').val();
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();
            var precio  = $(this).val();

            calculateTotalLine(desc,qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputIva').on('change',function(){
            var desc    = $(this).parent().siblings('.desc').find('input').val();
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).val();

            calculateTotalLine(desc,qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputDesc').on('change',function(){
            var desc    = $(this).val();
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();

            calculateTotalLine(desc,qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.productoSelect').on('change',function(){
            var $select  = $(this).parent().parent().siblings('.formatDiv').find('select');
            var $select2 = $(this).parent().parent().siblings('.prize').find('.inputPrize');
            var formato  = $(this).parent().parent().siblings('.formatDiv').find('.bootstrap-select').find('.productoSelect').val();
            if($(this).val() > 0){
                $.ajax({
                    url: "{{ url('/ajax/formatos') }}",
                    data: { enviado:1,idProducto:$(this).val(),idFormato:$(this).val(),"_token":"{{ csrf_token() }}" },
                    type: "post",
                    success: function(data){
                        $select.html(data.html);
                        $select2.val(data.precio);
                        $('.selectpicker').selectpicker('refresh');
                    }
                });
                $(this).parent().parent().siblings('.desc').find('input').val('');
                $(this).parent().parent().siblings('.qty').find('input').val('');
                $(this).parent().parent().siblings('.totalLine').find('input').val('0.00');
                calculateTotals();
            }else{
                $(this).parent().parent().siblings('.formatDiv').find('select').empty();
            }
        });

        $('.formatoSelect').on('change',function(){
            var precio  = $(this).parent().parent().siblings('.prize').find('input').val();
            var iva     = $(this).parent().parent().siblings('.iva').find('input').val();
            var $select = $(this).parent().parent().siblings('.prize').find('.inputPrize');
            var prod    = $(this).parent().parent().siblings('.productoDiv').find('.bootstrap-select').find('.productoSelect').val();
            if($(this).val() > 0){
                $.ajax({
                    url: "{{ url('/ajax/precio') }}",
                    data: { enviado:1,idProducto:prod,idFormato:$(this).val(),"_token":"{{ csrf_token() }}" },
                    type: "post",
                    success: function(data){
                        $select.val(data.html);
                        $('.selectpicker').selectpicker('refresh');
                    }
                });
                $(this).parent().parent().siblings('.desc').find('input').val('');
                $(this).parent().parent().siblings('.qty').find('input').val('');
                $(this).parent().parent().siblings('.totalLine').find('input').val('0.00');
                calculateTotals();
            }else{
                $(this).parent().parent().siblings('.prize').find('.inputPrize').val('0.00');
            }
        });

    }

    function calculateTotalLine(desc,qty,prize,iva,element){
        var total = parseFloat((1 - desc/100) * qty * prize * (1+iva/100)).toFixed(2);
        element.val(total);

        calculateTotals();
    }
    
    function calculateTotals() {
        var suma_subtotal   = 0.00;
        var iva_total       = 0.00;
        var suma_total      = 0.00;
        $('.inputTotal').each(function() {
            var descLinea   = $(this).parent().siblings('.desc').find('input').val();
            var precioLinea = $(this).parent().siblings('.prize').find('input').val();
            var ivaLinea    = $(this).parent().siblings('.iva').find('input').val();
            var qtyLinea    = $(this).parent().siblings('.qty').find('input').val();

            suma_subtotal   += (1 - descLinea/100) * precioLinea * qtyLinea;
            iva_total       += (1 - descLinea/100) * precioLinea * qtyLinea * (ivaLinea/100);
            suma_total      += (1 - (descLinea/100)) * precioLinea * qtyLinea * (1 + (ivaLinea/100));
        });
        $('.suma_subtotal').html(parseFloat(suma_subtotal).toFixed(2) + ' €');
        $('.suma_iva').html(parseFloat(iva_total).toFixed(2) + ' €');
        $('.suma_total').html(parseFloat(suma_total).toFixed(2) + ' €');
    }
</script>
@endpush