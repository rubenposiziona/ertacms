@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>
<?php $disabled = []; if ($pedido->estado != 1 && $pedido->estado != NULL) { $disabled = ['disabled' => 'disabled']; } ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            @if($pedido->id)
                <h1>Pedido nº {{ $pedido->num_pedido }}</h1>
            @else
                <h1>Nuevo pedido</h1>
            @endif

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/ventas') }}">Ventas</a></li>
                @if($pedido->id)
                <li class="active">Pedido nº {{ $pedido->num_pedido }}</li>
                @else
                <li class="active">Nuevo pedido</li>
                @endif
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        @if($pedido->estado == 1)
        <div class="btn-toolbar pull-right">
            <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        @elseif ($pedido->estado == 2 || $pedido->estado == 5)
        <div class="btn-toolbar pull-right">
            <a href="{{ url('/pdf/pedido/'.Crypt::encrypt($pedido->id)) }}" target="_blank" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Ver albarán</a>
        </div>
        @endif
        @if ($pedido->estado == 2)
        {{ Form::open(['url' => 'ventas/generar-factura', 'method' => 'post']) }}
            {{ Form::hidden('id_pedido', $pedido->id) }}
            <div class="btn-toolbar pull-right">
                <button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Generar factura</button>
            </div>
        {{ Form::close() }}
        @endif
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @if($pedido->estado == 1)
            {{ Form::open(['id' => 'nuevo_pedido', 'url' => 'ventas/ficha-pedido', 'method' => 'post']) }}
                {{ Form::hidden('id_pedido', $pedido->id) }}
            @endif
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group pull-right">
                        <h2 class="text-right" style="margin-top:0;">TOTAL</h2>
                        <h3 class="text-right suma_total" style="margin-top:0;">{{ money_format('%.2n',$pedido->total) }}</h3>
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group pull-right">
                        {{ Form::label('estado', 'Estado del pedido')}}
                        {{ Form::select('estado', $estados, $pedido->estado, ['class' => 'form-control selectpicker']+$disabled) }}
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('cliente', 'Cliente')}}
                        {{ Form::select('cliente', ['-1' => 'Selecciona un cliente'] + $clientes, $pedido->cliente, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']+$disabled) }}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('fecha_pedido', 'Fecha pedido')}}
                        {!! Form::text('fecha_pedido', $pedido->fechaPedidoFormat, ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('fecha_entrega', 'Fecha entrega')}}
                        {!! Form::text('fecha_entrega', $pedido->fechaEntregaFormat, ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <hr>

                <div id="lineas_pedido">
                    <h4>Líneas pedido</h4>
                    @foreach($lineasPedido as $linea)
                    <div class="linea_p">
                        {{ Form::hidden('idLinea[]', $linea->id) }}
                        <div class="col-sm-6 col-xs-12 form-group nopadding productoDiv">
                            {{ Form::label('producto', 'Producto')}}
                            {{ Form::select('producto[]', ['-1' => 'Selecciona un producto'] + $productos, $linea->id_producto, ['class' => 'form-control nobradius selectpicker productoSelect', 'data-live-search' => 'true']+$disabled) }}
                        </div>
                        <div class="col-sm-2 col-xs-12 form-group nopadding formatDiv">
                            {{ Form::label('formato', 'Formato')}}
                            {{ Form::select('formato[]', $linea->formatos, $linea->id_formato, ['class' => 'form-control nobradius formatoSelect selectpicker']+$disabled) }}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding qty">
                            {{ Form::label('cantidad', 'Cantidad')}}
                            {!! Form::text('cantidad[]', $linea->cantidad, ['class' => 'form-control nobradius text-right inputQty']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding prize">
                            {{ Form::label('precio', 'Precio')}}
                            {!! Form::text('precio[]', number_format($linea->precio, 2, '.', ''), ['class' => 'form-control nobradius text-right inputPrize']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding iva">
                            {{ Form::label('impuesto', 'Imp. (%)')}}
                            {!! Form::text('impuesto[]', number_format($linea->impuesto, 2, '.', ''), ['class' => 'form-control nobradius text-right inputIva']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding totalLine">
                            {{ Form::label('total', 'Total')}}<br>
                            {!! Form::text('total[]', number_format($linea->total, 2, '.', ''), ['class' => 'form-control nobradius text-right inputTotal', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach
                </div>

                @if($pedido->estado == 1)
                <div class="col-xs-12 nopadding">
                    {!! Form::button('<i class="fa fa-plus"></i> Añadir nueva línea', ['class' => 'btn btn-danger', 'onclick' => 'addLine(); return false;'] )  !!}
                </div>
                @endif
                <div class="clearfix"></div>
                <hr>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_subtotal"><?=money_format('%.2n',$pedido->subtotal)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>SUBTOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_iva"><?=money_format('%.2n',$pedido->iva)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>IVA</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_total"><?=money_format('%.2n',$pedido->total)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>TOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <hr>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {{ Form::label('observaciones', 'Observaciones')}}
                        {!! Form::textarea('observaciones', $pedido->observaciones, ['class' => 'form-control', 'rows' => '2']+$disabled) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <h4>Adjuntar archivo</h4>
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group">
                        {{ Form::label('titulo_arch', 'Título')}}
                        {!! Form::text('titulo_arch','', ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group">
                        {{ Form::label('archivo', 'Archivo')}}
                        {!! Form::file('archivo') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            @if($pedido->estado == 1)
            {{ Form::close() }}
            @endif

        </div>

        @if($pedido->estado == 1)
        <div class="btn-toolbar pull-right">
            <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        @endif
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $( "#fecha_pedido" ).datepicker({autoclose:true});
        $( "#fecha_entrega" ).datepicker({autoclose:true});

        $(".btn_save").on('click', function(e){
            e.preventDefault();
            $('#nuevo_pedido').submit();
        });

       loadOperations();
    });

    function addLine() {

        var content = '<div class="linea_p">{{ Form::hidden("idLinea[]", "") }}<div class="col-sm-6 col-xs-12 form-group nopadding productoDiv">{{ Form::label("producto", "Producto")}}{{ Form::select("producto[]", ["-1" => "Selecciona un producto"] + $productos, "", ["class" => "form-control nobradius productoSelect selectpicker", "data-live-search" => "true"]) }}</div><div class="col-sm-2 col-xs-12 form-group nopadding formatDiv">{{ Form::label("formato", "Formato")}}{{ Form::select("formato[]", array(), "", ["class" => "form-control nobradius formatoSelect selectpicker"]) }}</div><div class="col-sm-1 col-xs-12 form-group nopadding qty">{{ Form::label("cantidad", "Cantidad")}}{!! Form::text("cantidad[]", "", ["class" => "form-control nobradius text-right inputQty"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding prize">{{ Form::label("precio", "Precio")}}{!! Form::text("precio[]", number_format(0, 2, ".", ""), ["class" => "form-control nobradius text-right inputPrize"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding iva">{{ Form::label("impuesto", "Imp. (%)")}}{!! Form::text("impuesto[]", number_format(21, 2, ".", ""), ["class" => "form-control nobradius text-right inputIva"]) !!}</div><div class="col-sm-1 col-xs-12 form-group nopadding totalLine">{{ Form::label("total", "Total")}}<br>{!! Form::text("total[]", number_format(0, 2, ".", ""), ["class" => "form-control nobradius text-right inputTotal", "readonly" => "readonly"]) !!}</div><div class="clearfix"></div></div>';

            $('#lineas_pedido').append(content);
            $('.selectpicker').selectpicker('refresh');
            loadOperations();
            return false;
    }

    function loadOperations() {
        $('.inputQty').on('change',function(){
            var qty     = $(this).val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();

            calculateTotalLine(qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputPrize').on('change',function(){
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var iva     = $(this).parent().siblings('.iva').find('input').val();
            var precio  = $(this).val();

            calculateTotalLine(qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.inputIva').on('change',function(){
            var qty     = $(this).parent().siblings('.qty').find('input').val();
            var precio  = $(this).parent().siblings('.prize').find('input').val();
            var iva     = $(this).val();

            calculateTotalLine(qty,precio,iva,$(this).parent().siblings('.totalLine').find('input'));
        });

        $('.productoSelect').on('change',function(){
            var $select  = $(this).parent().parent().siblings('.formatDiv').find('.bootstrap-select').find('select');
            var $select2 = $(this).parent().parent().siblings('.prize').find('.inputPrize');
            var formato  = $(this).parent().parent().siblings('.formatDiv').find('.productoSelect').val();
            if($(this).val() > 0){
                $.ajax({
                    url: "{{ url('/ajax/formatos') }}",
                    data: { enviado:1,idProducto:$(this).val(),idFormato:$(this).val(),"_token":"{{ csrf_token() }}" },
                    type: "post",
                    success: function(data){
                        $select.html(data.html);
                        $select2.val(data.precio);
                        $('.selectpicker').selectpicker('refresh');
                    }
                });
                $(this).parent().parent().siblings('.qty').find('input').val('');
                $(this).parent().parent().siblings('.totalLine').find('input').val('0.00');
                calculateTotals();
            }else{
                $(this).parent().parent().siblings('.formatDiv').find('select').empty();
            }
        });

        $('.formatoSelect').on('change',function(){
            var $select = $(this).parent().parent().siblings('.prize').find('.inputPrize');
            var prod    = $(this).parent().parent().siblings('.productoDiv').find('.bootstrap-select').find('.productoSelect').val();
            if($(this).val() > 0){
                $.ajax({
                    url: "{{ url('/ajax/precio') }}",
                    data: { enviado:1,idProducto:prod,idFormato:$(this).val(),"_token":"{{ csrf_token() }}" },
                    type: "post",
                    success: function(data){
                        $select.val(data.html);
                        $('.selectpicker').selectpicker('refresh');
                    }
                });
                $(this).parent().parent().siblings('.qty').find('input').val('');
                $(this).parent().parent().siblings('.totalLine').find('input').val('0.00');
                calculateTotals();
            }else{
                $(this).parent().parent().siblings('.prize').find('.inputPrize').val('0.00');
            }
        });
    }

    function calculateTotalLine(qty,prize,iva,element){
        var total = parseFloat(qty * prize * (1+iva/100)).toFixed(2);
        element.val(total);

        calculateTotals();
    }
    
    function calculateTotals() {
        var suma_subtotal   = 0.00;
        var iva_total       = 0.00;
        var suma_total      = 0.00;
        $('.inputTotal').each(function() {
            var precioLinea = $(this).parent().siblings('.prize').find('input').val();
            var ivaLinea    = $(this).parent().siblings('.iva').find('input').val();
            var qtyLinea    = $(this).parent().siblings('.qty').find('input').val();

            suma_subtotal   += precioLinea * qtyLinea;
            iva_total       += precioLinea * qtyLinea * (ivaLinea/100);
            suma_total      += precioLinea * qtyLinea * (1 + (ivaLinea/100));
        });
        $('.suma_subtotal').html(parseFloat(suma_subtotal).toFixed(2) + ' Eu');
        $('.suma_iva').html(parseFloat(iva_total).toFixed(2) + ' Eu');
        $('.suma_total').html(parseFloat(suma_total).toFixed(2) + ' Eu');
    }
</script>
@endpush