@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Ventas</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Ventas</li>
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/ventas/ficha-presupuesto') }}" class="btn btn-primary">Nuevo presupuesto</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12" style="margin-top:40px;">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="budgets-table">
                    <thead>
                        <tr>
                            <th class="text-center">Nº Presupuesto</th>
                            <th class="text-center">Estado</th>
                            <th class="text-center">Fecha pedido</th>
                            <th class="text-center">Válido hasta</th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($presupuestos as $presupuesto)
                            <?php
                            $background = array('1' => 'warning', '2' => 'success', '3' => 'danger', '4' => 'danger', '5' => 'info');
                            ?>
                            <tr class="<?=$background[$presupuesto->estado]?>">
                                <td class="text-center">{{ $presupuesto->num_presupuesto }}</td>
                                <td class="text-center">{{ $presupuesto->state()->estado }}</td>
                                <td class="text-center">{{ $presupuesto->fecha->format('d/m/Y') }}</td>
                                <td class="text-center">{{ $presupuesto->valido_hasta->format('d/m/Y') }}</td>
                                <td>{{ $presupuesto->client()->nombre }}</td>
                                <td class="text-right">{{ money_format('%.2n',$presupuesto->total) }}</td>
                                <td class="text-center"><a href="{{ url('/ventas/ficha-presupuesto/'.Crypt::encrypt($presupuesto->id)) }}"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#budgets-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
    });

</script>
@endpush