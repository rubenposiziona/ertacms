@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>
<?php $disabled = ['disabled' => 'disabled']; ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            @if($presupuesto->id)
                <h1>Presupuesto nº {{ $presupuesto->num_presupuesto }}</h1>
            @else
                <h1>Nuevo presupuesto</h1>
            @endif

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/ventas') }}">Ventas</a></li>
                <li><a href="{{ url('/ventas/ficha-presupuesto/'.Crypt::encrypt($presupuesto->id)) }}">Presupuesto nº {{ $presupuesto->num_presupuesto }} <small>(v.{{ $presupuesto->version }})</small></a></li>
                <li class="active">Histórico versiones</li>
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        @foreach($presupuestos as $presup)

            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group pull-right">
                        <h2 class="text-right" style="margin-top:0;">TOTAL</h2>
                        <h3 class="text-right suma_total" style="margin-top:0;">{{ money_format('%.2n',$presup->total) }}</h3>
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group pull-right">
                        <h2>Versión {{ $presup->version }}</h2>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('cliente', 'Cliente')}}
                            {!! Form::text('cliente[]', $presupuesto->client()->nombre, ['class' => 'form-control nobradius']+$disabled) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('fecha', 'Fecha')}}
                        {!! Form::text('fecha', date("d/m/Y", strtotime($presup->fecha)), ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('valido_hasta', 'Válido hasta')}}
                        {!! Form::text('valido_hasta', date("d/m/Y", strtotime($presup->valido_hasta)), ['class' => 'form-control']+$disabled) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="lineas_pedido">
                    <h4>Líneas presupuesto</h4>
                    @foreach($presup->linea as $linea)
                    <div class="linea_p">
                        <div class="col-sm-6 col-xs-12 form-group nopadding productoDiv">
                            {{ Form::label('producto', 'Producto')}}
                            {!! Form::text('producto[]', $linea->product()->referencia, ['class' => 'form-control nobradius']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding formatDiv">
                            {{ Form::label('formato', 'Formato')}}
                            {!! Form::text('formato[]', $linea->format()->referencia, ['class' => 'form-control nobradius']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding desc">
                            {{ Form::label('descuento', 'Desc. (%)')}}
                            {!! Form::text('descuento[]', $linea->descuento, ['class' => 'form-control nobradius text-right']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding qty">
                            {{ Form::label('cantidad', 'Cantidad')}}
                            {!! Form::text('cantidad[]', $linea->cantidad, ['class' => 'form-control nobradius text-right']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding prize">
                            {{ Form::label('precio', 'Precio')}}
                            {!! Form::text('precio[]', $linea->precio, ['class' => 'form-control nobradius text-right']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding iva">
                            {{ Form::label('impuesto', 'Imp. (%)')}}
                            {!! Form::text('impuesto[]', $linea->impuesto, ['class' => 'form-control nobradius text-right']+$disabled) !!}
                        </div>
                        <div class="col-sm-1 col-xs-12 form-group nopadding totalLine">
                            {{ Form::label('total', 'Total')}}<br>
                            {!! Form::text('total[]', $linea->total, ['class' => 'form-control nobradius text-right']+$disabled) !!}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_subtotal"><?=money_format('%.2n',$presup->subtotal)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>SUBTOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_iva"><?=money_format('%.2n',$presup->iva)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>IVA</strong>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong class="suma_total"><?=money_format('%.2n',$presup->total)?></strong>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                    <strong>TOTAL</strong>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {{ Form::label('observaciones', 'Observaciones')}}
                        {!! Form::textarea('observaciones', $presup->observaciones, ['class' => 'form-control', 'rows' => '2']+$disabled) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="clearfix"></div>

            <hr>

        @endforeach

    </div>
</div>
@endsection