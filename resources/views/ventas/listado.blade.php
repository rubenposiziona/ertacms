@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Ventas</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Ventas</li>
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/ventas/ficha-pedido') }}" class="btn btn-primary">Nuevo pedido</a>
            <a href="{{ url('/ventas/ficha-presupuesto') }}" class="btn btn-primary">Nuevo presupuesto</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-4 col-xs-12">
            <button class="btn btn-danger" style="width:100%; margin-bottom: 10px;">
                <label class="btn-block">FACTURAS PENDIENTES PAGO</label>
                <h2>0,00 €</h2>
                <small>0 facturas</small>
            </button>
        </div>
        <div class="col-md-4 col-xs-12">
            <button class="btn btn-warning" style="width:100%; margin-bottom: 10px;">
                <label class="btn-block">PEDIDOS PENDIENTES</label>
                <h2>{{ money_format('%.2n',$total->sumPending) }}</h2>
                <small>{{ $total->numPending }} pedidos</small>
            </button>
        </div>
        <div class="col-md-4 col-xs-12">
            <button class="btn btn-success" style="width:100%; margin-bottom: 10px;">
                <label class="btn-block">TOTAL VENTAS</label>
                <h2>{{ money_format('%.2n',$total->sumAccept) }}</h2>
                <small>{{ $total->numAccept }} pedidos</small>
            </button>
        </div>
        <div class="clearfix"></div>

        <ul class="nav nav-tabs nav-justified" style="margin-top:40px;">
            <li><a href="#portlet_presupuestos" data-toggle="tab">Presupuestos</a></li>
            <li class="active"><a href="#portlet_pedidos" data-toggle="tab">Pedidos</a></li>
            <li><a href="#portlet_albaranes" data-toggle="tab">Albaranes</a></li>
        </ul>

        <div class="tab-content clearfix">
            <div class="tab-pane" id="portlet_presupuestos">
                
                <div class="col-xs-12" style="margin-top:40px;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="budgets-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Nº Presupuesto</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Fecha pedido</th>
                                    <th class="text-center">Válido hasta</th>
                                    <th class="text-center">Cliente</th>
                                    <th class="text-center">Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($presupuestos as $presupuesto)
                                    <?php $background = array('1' => 'warning', '2' => 'success', '3' => 'danger', '4' => 'danger', '5' => 'info'); ?>
                                    <tr class="<?=$background[$presupuesto->estado]?>">
                                        <td class="text-center">{{ $presupuesto->num_presupuesto }}</td>
                                        <td class="text-center">{{ $presupuesto->state()->estado }}</td>
                                        <td class="text-center">{{ $presupuesto->fecha->format('d/m/Y') }}</td>
                                        <td class="text-center">{{ $presupuesto->valido_hasta->format('d/m/Y') }}</td>
                                        <td>{{ $presupuesto->client()->nombre }}</td>
                                        <td class="text-right">{{ money_format('%.2n',$presupuesto->total) }}</td>
                                        <td class="text-center"><a href="{{ url('/ventas/ficha-presupuesto/'.Crypt::encrypt($presupuesto->id)) }}"><i class="fa fa-pencil"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="tab-pane active" id="portlet_pedidos">
                
                <div class="col-xs-12" style="margin-top:40px;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="orders-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Nº Pedido</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Fecha pedido</th>
                                    <th class="text-center">Cliente</th>
                                    <th class="text-center">Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pedidos as $pedido)
                                    <?php $background = array('1' => 'warning', '2' => 'success', '3' => 'danger', '4' => 'danger', '5' => 'info'); ?>
                                    <tr class="<?=$background[$pedido->estado]?>">
                                        <td class="text-center">{{ $pedido->num_pedido }}</td>
                                        <td class="text-center">{{ $pedido->state()->estado }}</td>
                                        <td class="text-center">{{ $pedido->fecha_pedido->format('d/m/Y') }}</td>
                                        <td>{{ $pedido->client()->nombre }}</td>
                                        <td class="text-right">{{ money_format('%.2n',$pedido->total) }}</td>
                                        <td class="text-center"><a href="{{ url('/ventas/ficha-pedido/'.Crypt::encrypt($pedido->id)) }}"><i class="fa fa-pencil"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
            <div class="tab-pane" id="portlet_albaranes">

                <div class="col-xs-12" style="margin-top:40px;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="albaran-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Nº Pedido</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Fecha pedido</th>
                                    <th class="text-center">Cliente</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Albarán</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($albaranes as $albaran)
                                    <?php $background = array('1' => 'warning', '2' => 'success', '3' => 'danger', '4' => 'info', '5' => 'info'); ?>
                                    <tr class="<?=$background[$albaran->estado]?>">
                                        <td class="text-center">{{ $albaran->num_pedido }}</td>
                                        <td class="text-center">{{ $albaran->state()->estado }}</td>
                                        <td class="text-center">{{ $albaran->fecha_pedido->format('d/m/Y') }}</td>
                                        <td>{{ $albaran->client()->nombre }}</td>
                                        <td class="text-right">{{ money_format('%.2n',$albaran->total) }}</td>
                                        <td class="text-center">
                                            @if($albaran->estado == 2 || $albaran->estado == 5)
                                                <a href="{{ url('/pdf/pedido/'.Crypt::encrypt($albaran->id)) }}" target="_blank"><i class="fa fa-file-pdf-o alert-danger"></i></a>
                                            @endif
                                        </td>
                                        <td class="text-center"><a href="{{ url('/ventas/ficha-pedido/'.Crypt::encrypt($albaran->id)) }}"><i class="fa fa-pencil"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>

    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#orders-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
        $('#budgets-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
        $('#albaran-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
    });

</script>
@endpush