@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Ficha cliente</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/clientes') }}">Clientes</a></li>
                <li class="active">Ficha cliente</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @include('includes.errors')

            {{ Form::open(['id' => 'nuevo_cliente', 'url' => 'clientes/ficha-cliente', 'method' => 'post']) }}
                {{ Form::hidden('id_cliente', $cliente->id) }}
                <div class="row">

                    @role('admin')
                        <div class="col-sm-6 col-xs-12 form-group">
                            {{ Form::label('comercial', 'Comercial asociado')}}
                            {{ Form::select('comercial', ['' => 'Selecciona un comercial'] + $comercial, $cliente->comercial_asig, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) }}
                        </div>
                        <div class="clearfix"></div>
                    @endrole
                    @role('comercial')
                        {{ Form::hidden('comercial', $comercial) }}
                    @endrole

                    <div class="col-sm-9 col-xs-12 form-group">
                        {{ Form::label('nombre', 'Razón social')}}
                        {!! Form::text('nombre', $cliente->nombre, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('nif', 'NIF / CIF')}}
                        {!! Form::text('nif', $cliente->nif, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('nombre_comercial', 'Nombre comercial')}}
                        {!! Form::text('nombre_comercial', $cliente->nombre_comercial, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('email', 'Email')}}
                        {!! Form::text('email', $cliente->email, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 form-group">
                        {{ Form::label('direccion', 'Dirección')}}
                        {!! Form::text('direccion', $cliente->direccion, ['class' => 'form-control']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-2 col-xs-12 form-group">
                        {{ Form::label('codigo_postal', 'Cód. postal')}}
                        {!! Form::text('codigo_postal', $cliente->c_postal, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('poblacion', 'Población')}}
                        {!! Form::text('poblacion', $cliente->poblacion, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('provincia', 'Provincia')}}
                        {!! Form::text('provincia', $cliente->provincia, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group">
                        {{ Form::label('pais', 'País')}}
                        {{ Form::select('pais', ['' => 'Selecciona un país'] + $paises, $cliente->pais, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) }}
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('telefono', 'Teléfono')}}
                        {!! Form::text('telefono', $cliente->telefono, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('movil', 'Móvil')}}
                        {!! Form::text('movil', $cliente->movil, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        {{ Form::label('forma_pago', 'Forma de pago')}}
                        {{ Form::select('forma_pago', ['' => 'Selecciona un método de pago'] + $pago, $cliente->formaPago, ['class' => 'form-control selectpicker']) }}
                    </div>
                    <div class="clearfix"></div>

                </div>

                <div class="row">
                    <div class="col-xs-12 form-group">
                        {{ Form::label('observaciones', 'Observaciones')}}
                        {!! Form::textarea('observaciones', $cliente->observaciones, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <h4>Adjuntar archivo</h4>
                <div class="row">
                    <div class="col-sm-8 col-xs-12 form-group">
                        {{ Form::label('titulo_arch', 'Título')}}
                        {!! Form::text('titulo_arch','', ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-4 col-xs-12 form-group">
                        {{ Form::label('archivo', 'Archivo')}}
                        {!! Form::file('archivo') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

            {{ Form::close() }}

        </div>

        <div class="btn-toolbar pull-right">
            <a href="javascript:void(0)" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
	<script>
	    $(function() {
	        
	        $(".btn_save").on('click', function(){
	            $('#nuevo_cliente').submit();
	        });

	    });
	</script>
@endpush