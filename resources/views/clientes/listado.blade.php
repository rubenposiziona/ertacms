@extends('layouts.app')

@section('content')
<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Clientes</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Listado clientes</li>
            </ol>
            <div class="clearfix"></div>
        </div>

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/clientes/ficha-cliente') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo cliente</a>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            @include('includes.errors')
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="clientes-table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>NIF</th>
                            <th>Población</th>
                            <th>Teléfono</th>
                            <th></th>
                            @permission('delete-client')
                            <th></th>
                            @endpermission
                        </tr>
                    </thead>
                    <tbody>
                    		@foreach($clientes as $cliente)
                                <tr>
                                    <td>{{ $cliente->nombre_comercial }}</td>
                                    <td class="text-center">{{ $cliente->nif }}</td>
                                    <td class="text-center">{{ $cliente->poblacion }}</td>
                                    <td class="text-center">{{ $cliente->telefono }}</td>
                                    <td class="text-center"><a href="{{ url('/clientes/ficha-cliente/'.Crypt::encrypt($cliente->id)) }}"><i class="fa fa-pencil"></i></a></td>
                                    @permission('delete-client')
                                    <td class="text-center"><a href="#" onclick="deleteUser({{ $cliente->id }}); return false;"><i class="fa fa-close"></i></a></td>
                                    @endpermission
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
	<script>
	    $(function() {
	        $('#clientes-table').DataTable({
	            "language": {
	                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
	            },
	            select: true,
	        });
	    });

        @permission('delete-client')
	    function deleteUser(id_user) {
	        swal({
	            title: "¿Está seguro de eliminar el cliente?",
	            type: "error",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "¡Eliminar!",
	            cancelButtonText: "Cancelar",
	            closeOnConfirm: false
	        },
	        function(){
	            window.location.href="{{ url('/clientes/eliminar') }}" + "/" + id_user;
	        });
	    }
        @endpermission
	</script>
@endpush