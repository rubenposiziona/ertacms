@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>
<?php $disabled = []; if ($factura->estado == 2) { $disabled = ['disabled' => 'disabled']; } ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Factura nº {{ $factura->num_factura }}</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li><a href="{{ url('/facturas') }}">Facturas</a></li>
                <li class="active">Factura nº {{ $factura->num_factura }}</li>
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/pdf/factura/'.Crypt::encrypt($factura->id)) }}" target="_blank" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Ver factura</a>
            @if($factura->estado != 2)
                <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
            @endif
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">

            <div class="row">
                @if($factura->estado != 2)
                {{ Form::open(['id' => 'bill_form', 'url' => 'facturas/ficha-factura', 'method' => 'post']) }}
                    {{ Form::hidden('id_factura', $factura->id) }}
                @endif
                    <div class="col-sm-6 col-xs-12 form-group pull-right">
                        <h2 class="text-right" style="margin-top:0;">TOTAL</h2>
                        <h3 class="text-right suma_total" style="margin-top:0;">{{ money_format('%.2n',$factura->total) }}</h3>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        {{ Form::label('estado', 'Estado del pedido')}}
                        {{ Form::select('estado', $estados, $factura->estado, ['class' => 'form-control selectpicker']+$disabled) }}
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group pull-right">
                        {{ Form::label('forma_pago', 'Forma de pago')}}
                        {{ Form::select('forma_pago', ['-1' => 'Seleccione una forma de pago']+$pagos, $factura->formaPago, ['class' => 'form-control selectpicker']+$disabled) }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <div id="pago_parcial" <?php if ($factura->estado != 3) { ?>style="display:none;"<?php } ?>>
                            {{ Form::label('cantidad_parcial', 'Cantidad pagada')}}
                            {!! Form::text('cantidad_parcial', $factura->pago_parcial, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                @if($factura->estado != 2)
                {{ Form::close() }}
                @endif
                <hr>

                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('fecha_pedido', 'Fecha')}}
                    {!! Form::text('fecha_pedido', date("d/m/Y", strtotime(str_replace('-', '/', $factura->fecha_factura))), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('nombre', 'Nombre')}}
                    {!! Form::text('nombre', $factura->nombre, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('nombre_comercial', 'Nombre comercial')}}
                    {!! Form::text('nombre_comercial', $factura->nom_comercial, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('cif', 'CIF')}}
                    {!! Form::text('cif', $factura->cif, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="clearfix"></div>

                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('email', 'Email')}}
                    {!! Form::text('email', $factura->email, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('telefono', 'Teléfono')}}
                    {!! Form::text('telefono', $factura->telefono, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('movil', 'Móvil')}}
                    {!! Form::text('movil', $factura->movil, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('fax', 'Fax')}}
                    {!! Form::text('fax', $factura->fax, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="clearfix"></div>

                <div class="col-sm-4 col-xs-12 form-group">
                    {{ Form::label('direccion', 'Dirección')}}
                    {!! Form::text('direccion', $factura->direccion, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-2 col-xs-12 form-group">
                    {{ Form::label('c_postal', 'Código postal')}}
                    {!! Form::text('c_postal', $factura->c_postal, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('poblacion', 'Población')}}
                    {!! Form::text('poblacion', $factura->poblacion, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="col-sm-3 col-xs-12 form-group">
                    {{ Form::label('provincia', 'Provincia')}}
                    {!! Form::text('provincia', $factura->provincia, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <hr>

            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                <strong class="suma_subtotal"><?=money_format('%.2n',$factura->subtotal)?></strong>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                <strong>SUBTOTAL</strong>
            </div>
            <div class="clearfix"></div>

            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                <strong class="suma_iva"><?=money_format('%.2n',$factura->iva)?></strong>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                <strong>IVA</strong>
            </div>
            <div class="clearfix"></div>

            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                <strong class="suma_total"><?=money_format('%.2n',$factura->total)?></strong>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 pull-right">
                <strong>TOTAL</strong>
            </div>
            <div class="clearfix"></div>

            <hr>

        </div>

        <div class="btn-toolbar pull-right">
            <a href="{{ url('/pdf/factura/'.Crypt::encrypt($factura->id)) }}" target="_blank" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Ver factura</a>
            @if($factura->estado != 2)
                <a href="#" class="btn btn-success btn_save"><i class="fa fa-save"></i> Guardar</a>
            @endif
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {

        $(".btn_save").on('click', function(e){
            e.preventDefault();
            $('#bill_form').submit();
        });

        $('#estado').change(function(){
            if ($(this).val()==3) {
                $('#pago_parcial').slideDown();
            }else {
                $('#pago_parcial').slideUp();
            }
        });

    });
</script>
@endpush