@extends('layouts.app')

@section('content')

<?php setlocale(LC_MONETARY, 'es_ES.UTF-8'); ?>

<div class="contenedor">
    <div class="row">
        <div class="col-xs-12">

            <h1>Facturas</h1>

            <ol class="breadcrumb pull-right">
                <li><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="active">Facturas</li>
            </ol>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        @include('includes.errors')

        <div class="btn-toolbar pull-right">
            <!--<a href="#" class="btn btn-primary">Nueva factura</a>-->
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="bills-table">
                    <thead>
                        <tr>
                            <th class="text-center">Nº Fra.</th>
                            <th class="text-center">Estado</th>
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Total</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($facturas as $factura)
                            <tr>
                                <td class="text-center">{{ $factura->num_factura }}</td>
                                <td class="text-center">{{ $factura->state()->estado }}@if($factura->estado==3) ({{ money_format('%.2n',$factura->pago_parcial) }}) @endif</td>
                                <td class="text-center">{{ $factura->fecha_factura->format('d/m/Y') }}</td>
                                <td>{{ $factura->nombre }}</td>
                                <td class="text-right">{{ money_format('%.2n',$factura->total) }}</td>
                                <td class="text-center"><a href="{{ url('/pdf/factura/'.Crypt::encrypt($factura->id)) }}" target="_blank"><i class="fa fa-file-pdf-o alert-danger"></i></a></td>
                                <td class="text-center"><a href="{{ url('/facturas/ficha-factura/'.Crypt::encrypt($factura->id)) }}"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#bills-table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            select: true,

        });
    });

</script>
@endpush